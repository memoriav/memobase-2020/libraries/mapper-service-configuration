package ch.memobase.shacl

import org.apache.jena.graph.Graph
import org.apache.jena.rdf.model.Model
import org.apache.jena.riot.Lang
import org.apache.jena.riot.RDFDataMgr
import org.apache.jena.shacl.ShaclValidator
import org.apache.jena.shacl.Shapes
import org.apache.jena.shacl.lib.ShLib
import org.apache.logging.log4j.LogManager
import java.io.StringWriter


class RdfValidation(
    private val validationFile: String) {
    private val log = LogManager.getLogger(this::class.java)
    private val validator = ShaclValidator.get()

    fun validate(data: Model): Pair<Boolean, String> {
        val shapesGraph: Graph = RDFDataMgr.loadGraph(validationFile)
        val dataGraph: Graph = data.graph

        val shapes: Shapes = Shapes.parse(shapesGraph)

        val report = validator.validate(shapes, dataGraph)

        StringWriter().use {
            RDFDataMgr.write(it, report.graph, Lang.TTL)
            return Pair(report.conforms(), it.toString())
        }
    }
}