/*
 * mapper-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.builder

import ch.memobase.helpers.StringHelpers
import ch.memobase.mapping.KEYS
import ch.memobase.rdf.MB
import ch.memobase.rdf.NS
import ch.memobase.rdf.RDA
import ch.memobase.rdf.RICO
import org.apache.jena.rdf.model.Resource

class Record(
    sourceId: String,
    type: String,
    recordSetId: String,
    institutionId: String,
    hasSponsoringAgent: Boolean,
    isPublished: Boolean
) :
    RecordResource(institutionId) {

    private val id = recordSetId + "-" + StringHelpers.normalizeId(sourceId)
    override val resource: Resource = model.createResource(NS.mbr + id)

    init {
        addRdfType(RICO.Record)
        resource.addProperty(RICO.type, type)
        resource.addProperty(RICO.isOrWasPartOf, recordSetUri(recordSetId))
        resource.addProperty(RICO.hasOrHadHolder, institutionUri)
        addRicoConcept(KEYS.identifiers, RICO.Types.Identifier.main, listOf(literal(id)))
        if (hasSponsoringAgent) {
            resource.addProperty(RDA.hasSponsoringAgentOfResource, model.createResource(MB.memoriavInstitutionURI))
        }
        resource.addProperty(MB.isPublished, model.createTypedLiteral(isPublished))
    }

    fun addInstantiation(instantiation: Instantiation) {
        resource.addProperty(RICO.hasInstantiation, instantiation.resource)
    }

    private fun recordSetUri(id: String): Resource = model.createResource(uri(NS.mbrs, id))
}
