/*
 * mapper-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.builder

import ch.memobase.mapping.KEYS
import ch.memobase.rdf.DC
import ch.memobase.rdf.EBUCORE
import ch.memobase.rdf.NS
import ch.memobase.rdf.RDA
import ch.memobase.rdf.RDF
import ch.memobase.rdf.RICO
import ch.memobase.rdf.SKOS
import org.apache.jena.rdf.model.Literal
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.Resource
import org.apache.logging.log4j.LogManager

abstract class RecordResource(institutionId: String) : IResource {
    private val log = LogManager.getLogger("ResourceBuilder")

    val model: Model = ModelFactory.createDefaultModel()

    abstract val resource: Resource
    protected val institutionUri: Resource = institutionUri(institutionId)

    init {
        model.setNsPrefixes(NS.prefixMapping)
    }

    protected fun addRdfType(type: Resource) {
        resource.addProperty(RDF.type, type)
    }

    override fun addLiteral(property: String, value: Literal) {
        resource.addLiteral(KEYS.keysToPropertyMap[property], value)
    }

    override fun addRicoConcept(rdfType: String, ricoType: String, value: List<Literal>) {
        when (rdfType) {
            KEYS.titles ->
                addRicoConcept(RICO.Title, ricoType, RICO.title, value, RICO.hasOrHadTitle)
            KEYS.identifiers ->
                addRicoConcept(RICO.Identifier, ricoType, RICO.identifier, value, RICO.hasOrHadIdentifier)
            KEYS.languages ->
                addRicoConcept(RICO.Language, ricoType, RICO.name, value, RICO.hasOrHadLanguage)
            else -> throw Exception("Unknown rdf:type alias for rico:Concept $rdfType.")
        }
    }

    private fun addRicoConcept(
        rdfType: Resource,
        ricoType: String,
        valueProperty: Property,
        value: List<Literal>,
        resourceProperty: Property
    ) {
        val blank = model.createResource()
        blank.addProperty(RDF.type, rdfType)
        blank.addProperty(RICO.type, ricoType)
        value.forEach {
            blank.addProperty(valueProperty, it)
        }
        resource.addProperty(resourceProperty, blank)
    }

    override fun addRicoCarrierType(names: List<Literal>) {
        val blank = model.createResource()
        blank.addProperty(RDF.type, RICO.CarrierType)
        names.forEach {
            blank.addProperty(RICO.name, it)
        }
        resource.addProperty(RICO.hasCarrierType, blank)
    }

    override fun addSkosConcept(type: String, properties: List<Pair<String, Literal>>) {
        val blank = model.createResource()
        blank.addProperty(RDF.type, SKOS.Concept)
        for (property in properties) {
            blank.addProperty(SKOS.get(property.first), property.second)
        }
        when (type) {
            KEYS.genre -> resource.addProperty(EBUCORE.hasGenre, blank)
            KEYS.subject -> resource.addProperty(RICO.hasOrHadSubject, blank)
        }
    }

    override fun addPlace(type: String, properties: List<Pair<String, Literal>>) {
        val blank = model.createResource()
        blank.addProperty(RDF.type, RICO.Place)
        for (property in properties) {
            blank.addProperty(KEYS.keysToPropertyMap[property.first], property.second)
        }
        when (type) {
            KEYS.placeOfCapture -> resource.addProperty(RDA.hasPlaceOfCapture, blank)
            KEYS.relatedPlaces -> resource.addProperty(DC.spatial, blank)
        }
    }

    override fun addDate(property: String, value: String) {
        val blank = model.createResource()
        blank.addProperty(RDF.type, RICO.DateSet)
        blank.addProperty(RICO.expressedDate, literal(value))
        resource.addProperty(KEYS.keysToPropertyMap[property], blank)
    }

    override fun addDateToAgent(property: String, value: Literal, agent: Resource) {
        val blank = model.createResource()
        blank.addProperty(RDF.type, RICO.DateSet)
        blank.addProperty(RICO.expressedDate, value)
        blank.addProperty(KEYS.keysToInversePropertyMap[property], agent)
        agent.addProperty(KEYS.keysToPropertyMap[property], blank)
    }

    override fun addCreationRelation(
        relationType: String,
        relationName: List<Literal>,
        agentClass: String,
        properties: List<Pair<String, Literal>>
    ) {
        val relation = model.createResource()
        if (relationType in KEYS.relationTypeMap) {
            relation.addProperty(RICO.type, KEYS.relationTypeMap[relationType])
        } else {
            log.error("Unexpected relation type found: $relationType.")
            return
        }
        relationName.forEach {
            relation.addProperty(RICO.name, it)
        }
        relation.addProperty(RICO.creationRelationHasSource, resource)
        relation.addProperty(RDF.type, RICO.CreationRelation)
        val agent = model.createResource()
        agent.addProperty(RDF.type, KEYS.agentTypeMap[agentClass])
        agent.addProperty(RICO.agentIsTargetOfCreationRelation, relation)
        for (property in properties) {
            if (property.first in KEYS.agentDateProperties) {
                addDateToAgent(property.first, property.second, agent)
            } else {
                agent.addProperty(KEYS.keysToPropertyMap[property.first], property.second)
            }
        }
        relation.addProperty(RICO.creationRelationHasTarget, agent)
        resource.addProperty(RICO.recordResourceOrInstantiationIsSourceOfCreationRelation, relation)
    }

    override fun addAgent(relation: String, agentType: String, properties: List<Pair<String, Literal>>) {
        val blank = model.createResource()
        blank.addProperty(RDF.type, KEYS.agentTypeMap[agentType])
        for (property in properties) {
            blank.addProperty(KEYS.keysToPropertyMap[property.first], property.second)
        }
        resource.addProperty(KEYS.agentPropertiesMap[relation], blank)
    }

    override fun addRule(type: String, value: List<Pair<String, List<Literal>>>) {
        if (value.isNotEmpty()) {
            val blank = model.createResource()
            blank.addProperty(RDF.type, RICO.Rule)
            blank.addProperty(RICO.type, literal(type))
            value.forEach {
                it.second.forEach { literal ->
                    blank.addProperty(KEYS.keysToPropertyMap[it.first], literal)
                }
            }
            blank.addProperty(RICO.regulatesOrRegulated, resource)
            resource.addProperty(RICO.isOrWasRegulatedBy, blank)
        } else {
            log.error("Added no rule for type $type, because there were no content properties mapped.")
        }
    }

    override fun langLiteral(text: String, language: String): Literal = model.createLiteral(text.trim(), language)
    override fun literal(text: String): Literal = model.createLiteral(text.trim())

    protected fun uri(ns: String, name: String): String = ns + name
    private fun institutionUri(id: String): Resource = model.createResource(uri(NS.mbcb, id))
}
