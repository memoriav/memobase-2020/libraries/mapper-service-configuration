/*
 * mapper-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.builder

import ch.memobase.helpers.StringHelpers
import ch.memobase.mapping.KEYS
import ch.memobase.rdf.NS
import ch.memobase.rdf.RICO
import org.apache.jena.rdf.model.Resource

class Thumbnail(sourceId: String, recordSetId: String, institutionId: String, count: Int) :
    Instantiation(institutionId) {
    private val id = recordSetId + "-" + StringHelpers.normalizeId(sourceId) + "-" + count + "/derived"
    override val resource: Resource = model.createResource(NS.mbdo + id)

    init {
        addRdfType(RICO.Instantiation)
        resource.addProperty(RICO.type, RICO.Types.Instantiation.thumbnail)
        addRicoConcept(KEYS.identifiers, RICO.Types.Identifier.main, listOf(literal(id)))
    }
}
