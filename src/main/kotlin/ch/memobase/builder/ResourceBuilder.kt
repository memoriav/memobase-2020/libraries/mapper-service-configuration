/*
 * mapper-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.builder

import ch.memobase.exceptions.InvalidInputException
import ch.memobase.mapping.KEYS
import ch.memobase.mapping.MapperConfiguration
import ch.memobase.mapping.fields.ConstantField
import ch.memobase.mapping.fields.MappedAnnotationField
import com.beust.klaxon.JsonArray
import org.apache.jena.riot.RDFDataMgr
import org.apache.jena.riot.RDFFormat
import org.apache.logging.log4j.LogManager
import java.io.StringWriter

class ResourceBuilder(
    private val source: Map<String, Any>,
    private val config: MapperConfiguration,
    private val institutionId: String,
    private val recordSetId: String,
    private val isPublished: Boolean
) {
    private val log = LogManager.getLogger(this::class.java)
    private var record: Record? = null
    private var physicalObject: PhysicalObject? = null
    private var digitalObject: DigitalObject? = null
    private var thumbnail: Thumbnail? = null
    private var recordId = ""
    private var recordTypeValue = ""
    val errorMessages: MutableList<String> = mutableListOf()
    var isFatal = false

    fun extractRecordId(): ResourceBuilder {
        if (source.containsKey(config.uri)) {
            when (val id = source[config.uri]) {
                is String -> recordId = id
                is Int -> recordId = id.toString()
                is JsonArray<*> -> errorMessages.add(
                    "Found multiple values in the field '${config.uri}' for identifiers: ${
                        id.joinToString(
                            ", "
                        )
                    }."
                )
                null -> errorMessages.add("The value for id is 'null' in field '${config.uri}'.")
                else -> errorMessages.add("Invalid value '$id' for id in field '${config.uri}'.")
            }
        } else {
            errorMessages.add("No id for record found in field '${config.uri}'.")
        }
        return this
    }

    fun hasRecordId(): Boolean {
        return recordId.isNotEmpty()
    }

    fun extractRecordTypeValue(): ResourceBuilder {
        recordTypeValue = when (val recordType = config.recordType) {
            is MappedAnnotationField -> source[recordType.field].let {
                if (it !is String) {
                    errorMessages.add("No type for record in field '${recordType.field}' for source $source.")
                    ""
                } else {
                    it
                }
            }
            is ConstantField -> recordType.constant
        }
        return this
    }

    fun hasRecordType(): Boolean {
        return if (recordTypeValue.isEmpty()) false
        else if (!KEYS.validRecordTypeValues.contains(recordTypeValue)) {
            errorMessages.add("Record type $recordTypeValue is invalid. Must be one of ${KEYS.validRecordTypeValues}.")
            false
        } else {
            true
        }
    }

    fun generateRecord(): ResourceBuilder {
        record = Record(
            recordId,
            recordTypeValue,
            recordSetId,
            institutionId,
            config.sponsoredByMemoriav,
            isPublished
        )
        for (recordFieldMapper in config.recordFieldMappers) {
            try {
                recordFieldMapper.apply(source, record!!)
            } catch (ex: InvalidInputException) {
                errorMessages.add("Could not apply ${recordFieldMapper::class.simpleName} for record for field " +
                        "'${recordFieldMapper.getFieldName()}', because: ${ex::class.java}: ${ex.localizedMessage}.")
            } catch (ex: Exception) {
                errorMessages.add("Could not apply ${recordFieldMapper::class.simpleName} for record for field " +
                        "'${recordFieldMapper.getFieldName()}', because: ${ex::class.java}: ${ex.stackTrace.joinToString("\n")}.")
                isFatal = true
            }
        }

        return this
    }

    fun generatePhysicalObject(): ResourceBuilder {
        physicalObject =
            if (config.physicalFieldMappers.isNotEmpty()) {
                val physicalObject = PhysicalObject(recordId, recordSetId, institutionId, 1)
                config.physicalFieldMappers.forEach {
                    try {
                        it.apply(source, physicalObject)
                    } catch (ex: InvalidInputException) {
                        errorMessages.add(ex.localizedMessage + " Could not apply ${it::class.simpleName} for physical instantiation.")
                    } catch (ex: Exception) {
                        errorMessages.add(ex.localizedMessage + " Could not apply ${it::class.simpleName} for physical instantiation.")
                        isFatal = true
                    }
                }
                record?.addInstantiation(physicalObject)
                physicalObject.addRecord(record!!)
                physicalObject
            } else {
                null
            }
        return this
    }

    fun generateDigitalObject(): ResourceBuilder {
        digitalObject =
            if (config.digitalFieldMappers.isNotEmpty()) {
                val digitalObject = DigitalObject(recordId, recordSetId, institutionId, 1, config.hasProxyType)
                var count = 0
                digitalObject.resource.listProperties().forEach { _ ->
                    count += 1
                }
                config.digitalFieldMappers.forEach {
                    try {
                        it.apply(source, digitalObject)
                    } catch (ex: InvalidInputException) {
                        errorMessages.add(ex.localizedMessage + " Could not apply ${it::class.simpleName} for digital instantiation.")
                    } catch (ex: Exception) {
                        errorMessages.add(ex.localizedMessage + " Could not apply ${it::class.simpleName} for digital instantiation.")
                        isFatal = true
                    }
                }
                var countExpanded = 0
                digitalObject.resource.listProperties().forEach { _ ->
                    countExpanded += 1
                }
                if (count == countExpanded) {
                    val message =
                        "Removed digital object from resource as there are no properties extracted from the data."
                    log.warn(message)
                    errorMessages.add(message)
                    null
                } else {
                    digitalObject.addRecord(record!!)
                    record?.addInstantiation(digitalObject)
                    digitalObject
                }
            } else {
                null
            }
        return this
    }
    fun generateThumbnailObject(): ResourceBuilder {
        thumbnail =
            if (config.thumbnailFieldMappers.isNotEmpty()) {
                val thumbnail = Thumbnail(recordId, recordSetId, institutionId, 1)
                var count = 0
                thumbnail.resource.listProperties().forEach { _ ->
                    count += 1
                }
                config.thumbnailFieldMappers.forEach {
                    try {
                        it.apply(source, thumbnail)
                    } catch (ex: InvalidInputException) {
                        errorMessages.add(ex.localizedMessage + " Could not apply ${it::class.simpleName} for thumbnail instantiation.")
                    } catch (ex: Exception) {
                        errorMessages.add(ex.localizedMessage + " Could not apply ${it::class.simpleName} for thumbnail instantiation.")
                        isFatal = true
                    }
                }
                var countExpanded = 0
                thumbnail.resource.listProperties().forEach { _ ->
                    countExpanded += 1
                }
                if (count == countExpanded) {
                    val message =
                        "Removed thumbnail object from resource as there are no properties extracted from the data."
                    log.warn(message)
                    errorMessages.add(message)
                    null
                } else {
                    digitalObject.let {
                        if (it != null) {
                            thumbnail.addIsDerivedFromInstantiation(it)
                            it.addDerivedInstantiation(thumbnail)
                        } else {
                            val message =
                                "The record has a thumbnail but no digital object!"
                            log.warn(message)
                            errorMessages.add(message)
                        }
                    }
                    record.let {
                        if (it != null) {
                            it.addInstantiation(thumbnail)
                            thumbnail.addRecord(it)
                        } else {
                            val message =
                                "The record has a thumbnail but no record object!"
                            log.error(message)
                            errorMessages.add(message)
                        }
                    }
                    thumbnail
                }
            } else {
                null
            }
        return this
    }

    fun addDerivedConnection(): ResourceBuilder {
        if (physicalObject != null && digitalObject != null) {
            physicalObject?.addDerivedInstantiation(digitalObject!!)
            digitalObject?.addIsDerivedFromInstantiation(physicalObject!!)
        }
        return this
    }

    private fun hasPhysicalObject(): Boolean = physicalObject != null

    private fun hasDigitalObject(): Boolean = digitalObject != null
    private fun hasThumbnail(): Boolean = thumbnail != null

    fun writeRecord(format: RDFFormat): Triple<String, String, List<String>> {
        return StringWriter().use { writer ->
            RDFDataMgr.write(writer, record!!.model, format)
            if (hasPhysicalObject()) {
                RDFDataMgr.write(writer, physicalObject!!.model, format)
            }
            if (hasDigitalObject()) {
                RDFDataMgr.write(writer, digitalObject!!.model, format)
            }
            if (hasThumbnail()) {
                RDFDataMgr.write(writer, thumbnail!!.model, format)
            }
            Triple(record!!.resource.uri, writer.toString().trim(), errorMessages)
        }
    }
}
