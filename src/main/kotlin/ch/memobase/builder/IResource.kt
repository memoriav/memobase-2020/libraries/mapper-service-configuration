/*
 * mapper-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.builder

import org.apache.jena.rdf.model.Literal
import org.apache.jena.rdf.model.Resource

interface IResource {
    fun addLiteral(property: String, value: Literal)
    fun addRicoConcept(rdfType: String, ricoType: String, value: List<Literal>)
    fun addSkosConcept(type: String, properties: List<Pair<String, Literal>>)
    fun addPlace(type: String, properties: List<Pair<String, Literal>>)
    fun addDate(property: String, value: String)
    fun addDateToAgent(property: String, value: Literal, agent: Resource)
    fun addCreationRelation(relationType: String, relationName: List<Literal>, agentClass: String, properties: List<Pair<String, Literal>>)
    fun addRule(type: String, value: List<Pair<String, List<Literal>>>)
    fun addRicoCarrierType(names: List<Literal>)
    fun addAgent(relation: String, agentType: String, properties: List<Pair<String, Literal>>)
    fun langLiteral(text: String, language: String): Literal
    fun literal(text: String): Literal
}
