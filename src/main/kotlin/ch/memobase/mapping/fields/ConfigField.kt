/*
 * mapper-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.mapping.fields

import ch.memobase.mapping.fields.SourceElement.SimpleString
import ch.memobase.mapping.fields.SourceElement.StringList
import org.apache.jena.rdf.model.Literal
import org.apache.jena.rdf.model.ResourceFactory
import org.apache.logging.log4j.LogManager


sealed class ConfigField {
    sealed class AnnotationField : ConfigField() {
        sealed class SimpleAnnotationField : AnnotationField() {
            data class ConstantField(val key: String, val constant: String) : SimpleAnnotationField() {
                fun toLiteral(): Literal {
                    return literal(constant)
                }

                fun toLangLiteral(tag: String): Literal {
                    return langLiteral(constant, tag)
                }
            }

            sealed class MappedAnnotationField(val key: String, val field: String) : SimpleAnnotationField() {
                data class PrefixField(
                    val prefixKey: String,
                    val prefix: String,
                    val prefixField: String
                ) : MappedAnnotationField(prefixKey, prefixField) {
                    override fun toLiteral(value: String): Literal {
                        return literal(prefix + value.trim())
                    }

                    override fun toLangLiteral(value: String, tag: String): Literal {
                        return langLiteral(prefix + value.trim(), tag)
                    }
                }

                data class DirectMapField(val directKey: String, val directField: String) :
                    MappedAnnotationField(directKey, directField) {
                    override fun toLiteral(value: String): Literal {
                        return literal(value)
                    }

                    override fun toLangLiteral(value: String, tag: String): Literal {
                        return langLiteral(value, tag)
                    }
                }

                abstract fun toLiteral(value: String): Literal
                abstract fun toLangLiteral(value: String, tag: String): Literal
            }
        }

        sealed class ComplexAnnotationField : AnnotationField() {
            data class ListField(val key: String, val fields: List<SimpleAnnotationField>) : ComplexAnnotationField() {
                private val log = LogManager.getLogger(this::class.java)
                fun toLiterals(source: Map<String, Any>): List<List<Literal>> {
                    val resultList = mutableListOf<MutableList<Literal>>()
                    for (field in fields) {
                        when (field) {
                            is MappedAnnotationField ->
                                FieldParsers.unpackSource(field.key, field.field, source).let { value ->
                                    when (value) {
                                        is SimpleString ->
                                            if (resultList.size == 1) {
                                                resultList[0].add(field.toLiteral(value.value))
                                            } else {
                                                resultList.add(mutableListOf(field.toLiteral(value.value)))
                                            }
                                        is StringList ->
                                            value.value.forEachIndexed { index, s ->
                                                // This makes the assumption that each value within each field
                                                // is at the same index for a specific field.
                                                // empty string act as padding to keep the original order.
                                                if (s.isNotEmpty()) {
                                                    if (resultList.size >= index + 1) {
                                                        resultList[index].add(field.toLiteral(s))
                                                    } else {
                                                        resultList.add(mutableListOf(field.toLiteral(s)))
                                                    }
                                                }
                                            }
                                        else -> log.debug("No value found for field {}.", field)
                                    }
                                }
                            is ConstantField ->
                                // This will cause an issue if there is a constant value added for a field
                                // with multiple entries.
                                if (resultList.size == 1) {
                                    resultList[0].add(field.toLiteral())
                                } else {
                                    resultList.add(mutableListOf(field.toLiteral()))
                                }
                        }
                    }
                    return resultList
                }
            }

            data class LanguageField(val key: String, val fields: List<ch.memobase.mapping.fields.LanguagePair>) :
                ComplexAnnotationField() {
                private val log = LogManager.getLogger(this::class.java)
                fun toLangLiterals(source: Map<String, Any>): List<List<Literal>> {
                    val resultList = mutableListOf<MutableList<Literal>>()
                    for (languagePair in fields) {
                        val tag = languagePair.tag
                        languagePair.sources.forEach { field ->
                            when (field) {
                                is MappedAnnotationField ->
                                    FieldParsers.unpackSource(field.key, field.field, source).let { value ->
                                        when (value) {
                                            is SimpleString ->
                                                if (resultList.size == 1) {
                                                    resultList[0].add(field.toLangLiteral(value.value, tag))
                                                } else {
                                                    resultList.add(mutableListOf(field.toLangLiteral(value.value, tag)))
                                                }
                                            is StringList ->
                                                value.value.forEachIndexed { index, s ->
                                                    // This makes the assumption that each value within each field
                                                    // is at the same index for a specific field.
                                                    if (s.isNotEmpty()) {
                                                        if (resultList.size >= index + 1) {
                                                            resultList[index].add(field.toLangLiteral(s, tag))
                                                        } else {
                                                            resultList.add(mutableListOf(field.toLangLiteral(s, tag)))
                                                        }
                                                    }
                                                }
                                            else -> log.debug("No value found for field $field.")
                                        }
                                    }
                                is ConstantField ->
                                    // This will cause an issue if there is a constant value added for a field
                                    // with multiple entries.
                                    if (resultList.size == 1) {
                                        resultList[0].add(field.toLangLiteral(tag))
                                    } else {
                                        resultList.add(mutableListOf(field.toLangLiteral(tag)))
                                    }
                            }
                        }
                    }
                    return resultList
                }
            }
        }
    }

    protected fun langLiteral(text: String, language: String): Literal =
        ResourceFactory.createLangLiteral(text.trim(), language)

    protected fun literal(text: String): Literal = ResourceFactory.createStringLiteral(text.trim())
}

typealias AnnotationField = ConfigField.AnnotationField

typealias DirectMapField = ConfigField.AnnotationField.SimpleAnnotationField.MappedAnnotationField.DirectMapField
typealias PrefixField = ConfigField.AnnotationField.SimpleAnnotationField.MappedAnnotationField.PrefixField
typealias ConstantField = ConfigField.AnnotationField.SimpleAnnotationField.ConstantField
typealias SimpleAnnotationField = ConfigField.AnnotationField.SimpleAnnotationField
typealias MappedAnnotationField = ConfigField.AnnotationField.SimpleAnnotationField.MappedAnnotationField

typealias ListField = ConfigField.AnnotationField.ComplexAnnotationField.ListField
typealias LanguageField = ConfigField.AnnotationField.ComplexAnnotationField.LanguageField
typealias ComplexAnnotationField = ConfigField.AnnotationField.ComplexAnnotationField
