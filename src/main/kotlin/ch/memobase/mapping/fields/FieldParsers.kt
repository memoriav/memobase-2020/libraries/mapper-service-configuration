/*
 * mapper-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.mapping.fields

import ch.memobase.exceptions.InvalidInputException
import ch.memobase.exceptions.InvalidMappingException
import ch.memobase.mapping.KEYS
import ch.memobase.mapping.fields.SourceElement.*
import org.apache.logging.log4j.LogManager
import kotlin.jvm.Throws

@Suppress("UNCHECKED_CAST")
object FieldParsers {
    private val log = LogManager.getLogger(this::class.java)

    private fun parsePrefixField(key: String, values: Map<String, String>): PrefixField {
        return values[KEYS.prefixAnnotationField].let { field ->
            if (field != null) {
                values[KEYS.prefixAnnotationValue].let { value ->
                    if (value != null) {
                        PrefixField(key, value, field.trim())
                    } else {
                        throw InvalidMappingException("Section: $key. A 'value' subfield in prefix definition is required.")
                    }
                }
            } else {
                throw InvalidMappingException("Section: $key. A 'field' subfield in prefix definition is required.")
            }
        }
    }

    fun parseSimpleAnnotationField(key: String, item: Any): SimpleAnnotationField {
        return when (item) {
            is String -> DirectMapField(key, item.trim())
            is Map<*, *> -> {
                when {
                    item.containsKey(KEYS.constantAnnotation) -> {
                        if (item[KEYS.constantAnnotation] is String) {
                            ConstantField(key, item[KEYS.constantAnnotation] as String)
                        } else {
                            throw InvalidMappingException("The constant annotation '${KEYS.constantAnnotation}:' " +
                                    "requires a string as value for field $key.")
                        }
                    }
                    item.containsKey(KEYS.prefixAnnotation) -> {
                        try {
                            if (item[KEYS.prefixAnnotation] == null) {
                                throw InvalidMappingException("Prefix field for $key is malformed.")
                            } else {
                                parsePrefixField(key, item[KEYS.prefixAnnotation] as Map<String, String>)
                            }
                        } catch (ex: ClassCastException) {
                            throw InvalidMappingException("Invalid structure for prefix field in section $key.")
                        }
                    }
                    else -> {
                        throw InvalidMappingException("Invalid key ${item.keys} for a simple annotation field in section $key. Expected 'const' or 'prefix'.")
                    }
                }
            }
            else -> throw InvalidMappingException("Invalid value for a simple annotation field in section $key. Expected 'const' or 'prefix' or a direct map.")
        }
    }

    private fun parseFieldList(key: String, items: List<Any>): List<SimpleAnnotationField> {
        return items.map {
            parseSimpleAnnotationField(key, it)
        }
    }

    fun parseAnnotationField(field: String, entry: Map.Entry<String, Any?>): AnnotationField {
        when (val value = entry.value) {
            is String -> return DirectMapField(entry.key, value)
            is Map<*, *> -> {
                return if (value.containsKey(KEYS.constantAnnotation)) {
                    ConstantField(entry.key, value[KEYS.constantAnnotation] as String)
                } else if (value.containsKey(KEYS.prefixAnnotation)) {
                    try {
                        parsePrefixField(entry.key, value[KEYS.prefixAnnotation] as Map<String, String>)
                    } catch (ex: ClassCastException) {
                        throw InvalidMappingException("Invalid structure for prefix field in section ${entry.key}.")
                    }
                } else {
                    val mutableList = mutableListOf<LanguagePair>()
                    for (k in value.keys) {
                        if (k is String && listOf("de", "it", "fr").contains(k)) {
                            val resultAnnotations = when (val items = value[k]) {
                                is List<*> -> {
                                    parseFieldList(entry.key, items as List<Any>)
                                }
                                is Any -> listOf(parseSimpleAnnotationField(entry.key, items))
                                else -> throw InvalidMappingException("The language code $k field must have a value for field '$field'.")
                            }
                            if (resultAnnotations.isEmpty()) {
                                throw InvalidMappingException("No valid definitions found under language tag $k in section ${entry.key}")
                            }
                            mutableList.add(LanguagePair(k, resultAnnotations))
                        }
                    }
                    if (mutableList.isNotEmpty()) {
                        LanguageField(entry.key, mutableList)
                    } else {
                        throw InvalidMappingException("Unknown key values in field mapping: $value. Use 'const' or 'de', 'fr', 'it' language tags!")
                    }
                }
            }
            is List<*> -> return ListField(
                entry.key,
                parseFieldList(entry.key, value as List<Any>)
            )
            null -> throw InvalidMappingException("The field $field contains a key with a null value: ${entry.key}.")
            else -> throw InvalidMappingException("The field $field is unexpected structure: $entry")
        }
    }

    @Throws(InvalidMappingException::class)
    fun parseFieldWithKeyValidation(
        parent: String,
        entry: Map.Entry<String, Any>,
        list: List<String>
    ): AnnotationField {
        if (list.contains(entry.key)) {
            return parseAnnotationField(parent, entry)
        } else {
            throw InvalidMappingException("The section $parent does not allow the key: ${entry.key}. Use any of $list.")
        }
    }

    @Throws(InvalidMappingException::class)
    fun parseSubFieldProperties(
        parent: String,
        entries: Set<Map.Entry<String, Any>>,
        allowedSubFields: List<String>
    ): List<AnnotationField> {
        return entries.map { entry ->
            if (allowedSubFields.contains(entry.key)) {
                return@map parseAnnotationField(parent, entry)
            } else {
                throw InvalidMappingException("The section $parent only allows the following subfields: $allowedSubFields.")
            }
        }
    }

    /**
     * Unpacks the source data JSON. This is the only place in this project where the values are extracted from the source.
     * The SourceElement is a simple wrapper around all the possible states the source data is allowed to take.
     * The dot notation tells the parser if there is an object present in the source data. This is way property names may not
     * contain a dot.
     *
     * Currently only Strings, Lists of Strings and Objects can be parsed.
     * Objects may only contain Strings or List of Strings.
     */
    @Throws(InvalidInputException::class)
    fun unpackSource(key: String, field: String, source: Map<String, Any>): SourceElement {
        return if (field.contains('.')) {
            val fields = field.split('.')
            source[fields[0]].let { objectValue ->
                when (objectValue) {
                    is Map<*, *> -> {
                        if (objectValue.containsKey(fields[1])) {
                            unpackValue(key, field, objectValue[fields[1]])
                        } else {
                            Empty
                        }
                    }
                    is List<*> -> {
                        when {
                            objectValue.isEmpty() -> Empty
                            objectValue.size == 1 -> {
                                val item = objectValue[0]
                                if (item is Map<*, *>) {
                                    unpackValue(key, field, item[fields[1]])
                                } else {
                                    throw InvalidInputException("The value in field ${field[0]} from key $key inside of the array is " +
                                            "not an object as expected. Found $item.")
                                }
                            }
                            else -> {
                                val resultList = mutableListOf<String>()
                                for (item in objectValue) {
                                    if (item is Map<*, *>) {
                                        when (val unpackedValue = unpackValue(key, field, item[fields[1]])) {
                                            is SimpleString -> resultList.add(unpackedValue.value)
                                            is StringList -> resultList.addAll(unpackedValue.value)
                                            Empty -> {
                                                // Adding an empty value here because optional fields in objects
                                                // otherwise mess up the order. This means that empty fields need to be ignored.
                                                resultList.add("")
                                                log.debug("No or illegal value in map found for $key with target field $field.")
                                            }
                                        }
                                    }
                                }
                                StringList(resultList)
                            }
                        }
                    }
                    null -> Empty
                    else -> {
                        log.error("Could not parse object for field $field in source from key $key.")
                        Empty
                    }
                }
            }
        } else {
            unpackValue(key, field, source[field])
        }
    }

    /**
     * Unpacks the value from the source data.
     *
     * @param key the key of the source data.
     * @param field the field of the source data.
     * @param value the value of the source data.
     *
     * @throws InvalidInputException if the value is not a String, a List of Strings or null.
     * @return the value as a SourceElement.
     * @see SourceElement
     */
    @Throws(InvalidInputException::class)
    private fun unpackValue(key: String, field: String, value: Any?): SourceElement {
        return when (value) {
            is String -> SimpleString(value)
            is List<*> -> {
                if (value.isNotEmpty() && value[0] is String) {
                    StringList(value as List<String>)
                } else {
                    val message = "Could not parse list elements in field $field from key $key. It is either an empty list or elements are not strings. Found $value."
                    throw InvalidInputException(message)
                }
            }
            null -> Empty
            else -> {
                val message = "Could not parse element in field $field from key $key. Expected a string, a list of strings or a null value. Found $value."
                throw InvalidInputException(message)
            }
        }
    }
}
