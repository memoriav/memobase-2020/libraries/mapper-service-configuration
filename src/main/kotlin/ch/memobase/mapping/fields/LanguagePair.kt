package ch.memobase.mapping.fields

data class LanguagePair(
    val tag: String,
    val sources: List<SimpleAnnotationField>
)
