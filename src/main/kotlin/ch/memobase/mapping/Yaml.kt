/*
 * mapper-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.mapping

import ch.memobase.exceptions.InvalidMappingException
import ch.memobase.settings.CustomEnvConfig
import ch.memobase.settings.MissingSettingException
import java.io.ByteArrayInputStream
import java.util.Optional
import org.snakeyaml.engine.v2.api.Load
import org.snakeyaml.engine.v2.api.LoadSettings
import org.snakeyaml.engine.v2.exceptions.DuplicateKeyException
import org.snakeyaml.engine.v2.exceptions.MissingEnvironmentVariableException
import kotlin.jvm.Throws

@Suppress("UNCHECKED_CAST")
object Yaml {

    @Throws(MissingSettingException::class, InvalidMappingException::class)
    fun load(data: ByteArray): Map<String, Any?> {
        val settings =
            LoadSettings.builder().setEnvConfig(Optional.of(CustomEnvConfig())).build()
        val load = Load(settings)
        return try {
            load.loadFromInputStream(ByteArrayInputStream(data)) as Map<String, Any?>
        } catch (ex: MissingEnvironmentVariableException) {
            throw MissingSettingException("env", ex.localizedMessage)
        } catch (ex: ClassCastException) {
            throw InvalidMappingException("Invalid file structure encountered in mapping file.")
        } catch (ex: DuplicateKeyException) {
            // TODO: Figure out why this exception is not properly caught...
            throw InvalidMappingException("Duplicate Key Detected: ${ex.localizedMessage}.")
        }
    }
}
