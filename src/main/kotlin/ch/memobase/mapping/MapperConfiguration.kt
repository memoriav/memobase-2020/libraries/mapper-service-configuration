/*
 * mapper-service
 * Copyright (C) 2020-present Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.mapping

import ch.memobase.mapping.fields.SimpleAnnotationField
import ch.memobase.mapping.mappers.AbstractFieldMapper

data class MapperConfiguration(
    val uri: String,
    val sponsoredByMemoriav: Boolean,
    val hasProxyType: Boolean,
    val recordType: SimpleAnnotationField,
    val recordFieldMappers: List<AbstractFieldMapper>,
    val physicalFieldMappers: List<AbstractFieldMapper>,
    val digitalFieldMappers: List<AbstractFieldMapper>,
    val thumbnailFieldMappers: List<AbstractFieldMapper>
)
