/*
 * mapper-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.mapping.mappers

import ch.memobase.builder.IResource
import ch.memobase.mapping.fields.ConstantField
import ch.memobase.mapping.fields.DirectMapField
import ch.memobase.mapping.fields.LanguageField
import ch.memobase.mapping.fields.ListField
import ch.memobase.mapping.fields.PrefixField
import ch.memobase.rdf.SKOS

class SkosConceptFieldMapper : TypeFieldMapper() {
    override fun apply(source: Map<String, Any>, subject: IResource) {
        translateProperties(source)
        properties.forEach {
            if (it.any { value -> SKOS.labelProperties.contains(value.first) }) {
                subject.addSkosConcept(agentClassType, it)
            } else {
                log.warn("No skos:Concept was created because there was no valid SKOS label property present in the properties list.")
            }
        }
    }

    override fun getFieldName(): String {
        return fields[0].let {
            when (it) {
                is ConstantField -> it.key
                is PrefixField -> it.key
                is DirectMapField -> it.key
                is ListField -> it.key
                is LanguageField -> it.key
            }
        }
    }
}
