/*
 * mapper-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.mapping.mappers

import ch.memobase.builder.IResource
import ch.memobase.mapping.KEYS
import ch.memobase.mapping.fields.ConstantField
import ch.memobase.mapping.fields.DirectMapField
import ch.memobase.mapping.fields.LanguageField
import ch.memobase.mapping.fields.ListField
import ch.memobase.mapping.fields.PrefixField

class AgentFieldMapper : TypeFieldMapper() {
    var sourceKey: String = ""
    override fun apply(source: Map<String, Any>, subject: IResource) {
        translateProperties(source)
        properties.forEach { propertyList ->
            val relationNameField = propertyList.filter { it.first == KEYS.relationName }
            val otherFields = propertyList.filterNot { it.first == KEYS.relationName }
            if (otherFields.isNotEmpty()) {
                if (relationNameField.isEmpty() && KEYS.agentPropertiesMap.containsKey(sourceKey)) {
                    subject.addAgent(sourceKey, agentClassType, otherFields)
                } else {
                    val relationNameLiterals = relationNameField.map { it.second }
                    subject.addCreationRelation(sourceKey, relationNameLiterals, agentClassType, otherFields)
                }
            }
        }
    }

    override fun getFieldName(): String {
        return fields[0].let {
            when (it) {
                is ConstantField -> it.key
                is PrefixField -> it.key
                is DirectMapField -> it.key
                is ListField -> it.key
                is LanguageField -> it.key
            }
        }
    }
}
