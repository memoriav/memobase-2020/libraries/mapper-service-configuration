/*
 * mapper-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.mapping.mappers

import ch.memobase.builder.IResource
import ch.memobase.mapping.fields.LanguageField
import org.apache.jena.rdf.model.Literal

class LanguageFieldMapper(private val languageField: LanguageField) : AbstractFieldMapper() {
    override fun apply(source: Map<String, Any>, subject: IResource) {
        languageField.toLangLiterals(source).forEach { entityList: List<Literal> ->
            entityList.forEach { literal ->
                subject.addLiteral(languageField.key, literal)
            }
        }
    }

    override fun getFieldName(): String {
        return languageField.key
    }
}
