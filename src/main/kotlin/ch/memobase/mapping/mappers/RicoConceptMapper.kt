/*
 * mapper-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.mapping.mappers

import ch.memobase.builder.IResource
import ch.memobase.mapping.fields.AnnotationField
import ch.memobase.mapping.fields.ConstantField
import ch.memobase.mapping.fields.FieldParsers
import ch.memobase.mapping.fields.LanguageField
import ch.memobase.mapping.fields.ListField
import ch.memobase.mapping.fields.MappedAnnotationField
import ch.memobase.mapping.fields.SourceElement.Empty
import ch.memobase.mapping.fields.SourceElement.SimpleString
import ch.memobase.mapping.fields.SourceElement.StringList
import org.apache.logging.log4j.LogManager

class RicoConceptMapper(private val rdfType: String, val field: AnnotationField) :
    AbstractFieldMapper() {
    private val log = LogManager.getLogger(this::class.java)
    override fun apply(source: Map<String, Any>, subject: IResource) {
        when (field) {
            is MappedAnnotationField ->
                FieldParsers.unpackSource(field.key, field.field, source).let {
                    when (it) {
                        is SimpleString -> subject.addRicoConcept(rdfType, field.key, listOf(field.toLiteral(it.value)))
                        is StringList ->
                            it.value.forEach { value ->
                                if (value.isNotEmpty())
                                    subject.addRicoConcept(rdfType, field.key, listOf(field.toLiteral(value)))
                            }
                        is Empty -> log.debug("Found no element for field ${field.key}.")
                    }
                }
            is ConstantField ->
                subject.addRicoConcept(rdfType, field.key, listOf(field.toLiteral()))
            is LanguageField -> {
                val fields = field.toLangLiterals(source)
                fields.forEach {
                    subject.addRicoConcept(rdfType, field.key, it)
                }
            }
            is ListField -> {
                val fields = field.toLiterals(source)
                fields.forEach {
                    subject.addRicoConcept(rdfType, field.key, it)
                }
            }
        }
    }

    override fun getFieldName(): String {
        return when (field) {
            is MappedAnnotationField -> field.key
            is ConstantField -> field.key
            is LanguageField -> field.key
            is ListField -> field.key
        }
    }
}
