/*
 * mapper-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.mapping.mappers

import ch.memobase.builder.IResource
import ch.memobase.mapping.fields.ConfigField
import ch.memobase.mapping.fields.ConstantField
import ch.memobase.mapping.fields.DirectMapField
import ch.memobase.mapping.fields.FieldParsers
import ch.memobase.mapping.fields.LanguageField
import ch.memobase.mapping.fields.ListField
import ch.memobase.mapping.fields.MappedAnnotationField
import ch.memobase.mapping.fields.PrefixField
import ch.memobase.mapping.fields.SimpleAnnotationField
import ch.memobase.mapping.fields.SourceElement
import ch.memobase.mapping.fields.SourceElement.Empty
import ch.memobase.mapping.fields.SourceElement.SimpleString
import ch.memobase.mapping.fields.SourceElement.StringList
import org.apache.jena.rdf.model.Literal
import org.apache.logging.log4j.LogManager

class ExpandedRuleFieldMapper(
    private val ricoType: String,
    private val configField: List<ConfigField>
) : AbstractFieldMapper() {
    private val log = LogManager.getLogger(this::class.java)

    override fun apply(source: Map<String, Any>, subject: IResource) {
        val properties = mutableListOf<MutableList<Pair<String, List<Literal>>>>()
        configField.forEach { field ->
            when (field) {
                is MappedAnnotationField ->
                    FieldParsers.unpackSource(field.key, field.field, source).let { sourceElement: SourceElement ->
                        when (sourceElement) {
                            is SimpleString ->
                                if (properties.size == 1)
                                    properties[0].add(Pair(field.key, listOf(field.toLiteral(sourceElement.value))))
                                else
                                    properties.add(
                                        mutableListOf(
                                            Pair(
                                                field.key,
                                                listOf(field.toLiteral(sourceElement.value))
                                            )
                                        )
                                    )
                            is StringList ->
                                sourceElement.value.forEachIndexed { index, s ->
                                    if (s.isNotEmpty()) {
                                        if (properties.size == index + 1)
                                            properties[index].add(Pair(field.key, listOf(field.toLiteral(s))))
                                        else
                                            properties.add(
                                                index,
                                                mutableListOf(Pair(field.key, listOf(field.toLiteral(s))))
                                            )
                                    }
                                }
                            Empty -> log.debug("Could not find a valid value for field ${field.key}.")
                        }
                    }
                is ConstantField ->
                    if (properties.size == 1)
                        properties[0].add(Pair(field.key, listOf(field.toLiteral())))
                    else
                        properties.add(mutableListOf(Pair(field.key, listOf(field.toLiteral()))))
                is LanguageField -> {
                    val literals = field.toLangLiterals(source)
                    literals.forEachIndexed { index, list ->
                        if (properties.size == index + 1) {
                            properties[index].add(Pair(field.key, list))
                        } else
                            properties.add(index, mutableListOf(Pair(field.key, list)))
                    }
                }
                is ListField -> {
                    val literals = field.toLiterals(source)
                    literals.forEachIndexed { index, list ->
                        if (properties.size == index + 1)
                            properties[index].add(Pair(field.key, list))
                        else
                            properties.add(index, mutableListOf(Pair(field.key, list)))
                    }
                }
            }
        }
        properties.forEach {
            subject.addRule(ricoType, it)
        }
    }

    override fun getFieldName(): String {
        return configField[0].let {
            when (it) {
                is ConstantField -> it.key
                is PrefixField -> it.key
                is DirectMapField -> it.key
                is ListField -> it.key
                is LanguageField -> it.key
            }
        }
    }
}
