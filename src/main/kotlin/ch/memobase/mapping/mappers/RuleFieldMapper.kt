/*
 * mapper-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.mapping.mappers

import ch.memobase.builder.IResource
import ch.memobase.mapping.KEYS
import ch.memobase.mapping.fields.ConfigField
import ch.memobase.mapping.fields.ConstantField
import ch.memobase.mapping.fields.DirectMapField
import ch.memobase.mapping.fields.FieldParsers
import ch.memobase.mapping.fields.LanguageField
import ch.memobase.mapping.fields.ListField
import ch.memobase.mapping.fields.MappedAnnotationField
import ch.memobase.mapping.fields.PrefixField
import ch.memobase.mapping.fields.SourceElement.Empty
import ch.memobase.mapping.fields.SourceElement.SimpleString
import ch.memobase.mapping.fields.SourceElement.StringList
import org.apache.logging.log4j.LogManager

class RuleFieldMapper(private val configField: ConfigField) : AbstractFieldMapper() {
    private val log = LogManager.getLogger(this::class.java)
    override fun apply(source: Map<String, Any>, subject: IResource) {
        when (configField) {
            is MappedAnnotationField ->
                FieldParsers.unpackSource(configField.key, configField.field, source).let {
                    when (it) {
                        is SimpleString -> subject.addRule(
                            configField.key,
                            listOf(Pair(KEYS.name, listOf(configField.toLiteral(it.value))))
                        )
                        is StringList ->
                            it.value.forEach { value ->
                                if (value.isNotEmpty())
                                    subject.addRule(
                                        configField.key,
                                        listOf(Pair(KEYS.name, listOf(configField.toLiteral(value))))
                                    )
                            }
                        is Empty -> log.debug("Found no element for field ${configField.key}.")
                    }
                }
            is ConstantField ->
                subject.addRule(configField.key, listOf(Pair(KEYS.name, listOf(configField.toLiteral()))))
            is LanguageField -> {
                val literals = configField.toLangLiterals(source)
                literals.forEach {
                    subject.addRule(configField.key, listOf(Pair(KEYS.name, it)))
                }
            }
            is ListField -> {
                val literals = configField.toLiterals(source)
                literals.forEach {
                    subject.addRule(configField.key, listOf(Pair(KEYS.name, it)))
                }
            }
        }
    }

    override fun getFieldName(): String {
        return configField.let {
            when (it) {
                is ConstantField -> it.key
                is PrefixField -> it.key
                is DirectMapField -> it.key
                is ListField -> it.key
                is LanguageField -> it.key
            }
        }
    }
}
