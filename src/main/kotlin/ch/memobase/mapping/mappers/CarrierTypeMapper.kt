/*
 * mapper-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.mapping.mappers

import ch.memobase.builder.IResource
import ch.memobase.mapping.fields.AnnotationField
import ch.memobase.mapping.fields.ConstantField
import ch.memobase.mapping.fields.FieldParsers
import ch.memobase.mapping.fields.LanguageField
import ch.memobase.mapping.fields.ListField
import ch.memobase.mapping.fields.MappedAnnotationField
import ch.memobase.mapping.fields.SourceElement
import ch.memobase.mapping.fields.SourceElement.Empty
import ch.memobase.mapping.fields.SourceElement.SimpleString
import ch.memobase.mapping.fields.SourceElement.StringList
import org.apache.logging.log4j.LogManager

class CarrierTypeMapper(val field: AnnotationField) : AbstractFieldMapper() {
    private val log = LogManager.getLogger(this::class.java)
    override fun apply(source: Map<String, Any>, subject: IResource) {
        when (field) {
            is MappedAnnotationField ->
                FieldParsers.unpackSource(field.key, field.field, source).let { sourceElement: SourceElement ->
                    when (sourceElement) {
                        is SimpleString -> subject.addRicoCarrierType(listOf(field.toLiteral(sourceElement.value)))
                        is StringList ->
                            sourceElement.value.forEach {
                                if (it.isNotEmpty())
                                    subject.addRicoCarrierType(listOf(field.toLiteral(it)))
                            }
                        is Empty -> log.debug("No value for field ${field.key} in source data.")
                    }
                }
            is ConstantField ->
                subject.addRicoCarrierType(listOf(field.toLiteral()))
            is LanguageField -> {
                val fields = field.toLangLiterals(source)
                fields.forEach {
                    subject.addRicoCarrierType(it)
                }
            }
            is ListField -> {
                val fields = field.toLiterals(source)
                fields.forEach {
                    subject.addRicoCarrierType(it)
                }
            }
        }
    }

    override fun getFieldName(): String {
        return when (field) {
            is MappedAnnotationField -> field.key
            is ConstantField -> field.key
            is LanguageField -> field.key
            is ListField -> field.key
        }
    }
}
