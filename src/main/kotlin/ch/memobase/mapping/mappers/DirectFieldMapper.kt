/*
 * mapper-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.mapping.mappers

import ch.memobase.builder.IResource
import ch.memobase.mapping.fields.DirectMapField
import ch.memobase.mapping.fields.FieldParsers
import ch.memobase.mapping.fields.SourceElement
import ch.memobase.mapping.fields.SourceElement.*
import org.apache.logging.log4j.LogManager

class DirectFieldMapper(private val directMapField: DirectMapField, private val defaultValue: String? = null) :
    AbstractFieldMapper() {
    private val log = LogManager.getLogger(this::class.java)
    override fun apply(source: Map<String, Any>, subject: IResource) {
        FieldParsers.unpackSource(directMapField.key, directMapField.field, source)
            .let { sourceElement: SourceElement ->
                when (sourceElement) {
                    is SimpleString -> subject.addLiteral(
                        directMapField.key,
                        directMapField.toLiteral(sourceElement.value)
                    )

                    is StringList -> sourceElement.value.forEach {
                        if (it.isNotEmpty())
                            subject.addLiteral(directMapField.key, directMapField.toLiteral(it))
                    }

                    is Empty -> {
                        if (defaultValue != null) {
                            subject.addLiteral(directMapField.key, directMapField.toLiteral(defaultValue))
                        } else {
                            log.debug("Found no element for field ${directMapField.key}.")
                        }
                    }
                }
            }
    }

    override fun getFieldName(): String {
        return directMapField.directKey
    }
}
