/*
 * mapper-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.mapping.mappers

import ch.memobase.exceptions.InvalidInputException
import ch.memobase.mapping.fields.ConfigField
import ch.memobase.mapping.fields.ConstantField
import ch.memobase.mapping.fields.FieldParsers
import ch.memobase.mapping.fields.LanguageField
import ch.memobase.mapping.fields.ListField
import ch.memobase.mapping.fields.MappedAnnotationField
import ch.memobase.mapping.fields.SourceElement.Empty
import ch.memobase.mapping.fields.SourceElement.SimpleString
import ch.memobase.mapping.fields.SourceElement.StringList
import org.apache.jena.rdf.model.Literal
import org.apache.logging.log4j.LogManager

abstract class TypeFieldMapper : AbstractFieldMapper() {
    protected val log = LogManager.getLogger(this::class.java)
    var agentClassType = ""
    val fields = mutableListOf<ConfigField>()

    protected val properties = mutableListOf<MutableList<Pair<String, Literal>>>()

    fun setFields(type: String, fields: List<ConfigField>) {
        this.agentClassType = type
        this.fields.clear()
        this.fields.addAll(fields)
    }

    private fun addMappedField(field: MappedAnnotationField, source: Map<String, Any>) {
        FieldParsers.unpackSource(field.key, field.field, source).let { sourceElement ->
            when (sourceElement) {
                is SimpleString ->
                    if (properties.size == 1) {
                        properties[0].add(Pair(field.key, field.toLiteral(sourceElement.value)))
                    } else {
                        properties.add(mutableListOf(Pair(field.key, field.toLiteral(sourceElement.value))))
                    }
                is StringList ->
                    sourceElement.value.forEachIndexed { index, s ->
                        if (s.isNotEmpty()) {
                            if (properties.size >= index + 1) {
                                log.debug("Adding ${field.key} to properties list at existing index $index.")
                                properties[index].add(Pair(field.key, field.toLiteral(s)))
                            } else {
                                log.debug("Adding ${field.key} to properties list at new index $index.")
                                log.debug(properties)
                                try {
                                    properties.add(index, mutableListOf(Pair(field.key, field.toLiteral(s))))
                                } catch (ex: IndexOutOfBoundsException) {
                                    throw InvalidInputException("Encountered an unexpected empty value for mapping ${field.key} -> ${field.field}.")
                                }
                            }
                        } else {
                            log.debug("Empty value for index $index.")
                        }
                    }
                Empty -> log.debug("No valid value found for field ${field.key}.")
            }
        }
    }

    private fun addConstantField(field: ConstantField) {
        if (properties.size == 1) {
            properties[0].add(Pair(field.key, field.toLiteral()))
        } else {
            properties.add(mutableListOf(Pair(field.key, field.toLiteral())))
        }
    }

    protected fun translateProperties(source: Map<String, Any>) {
        properties.clear()
        for (field in fields) {
            when (field) {
                is ConstantField -> addConstantField(field)
                is MappedAnnotationField -> addMappedField(field, source)
                is LanguageField ->
                    field.toLangLiterals(source).forEach { item ->
                        item.forEachIndexed { index, literal ->
                            if (properties.size >= index + 1) {
                                properties[index].add(Pair(field.key, literal))
                            } else {
                                properties.add(mutableListOf(Pair(field.key, literal)))
                            }
                        }
                    }
                is ListField ->
                    field.toLiterals(source).forEach { item ->
                        item.forEachIndexed { index, literal ->
                            if (properties.size >= index + 1) {
                                properties[index].add(Pair(field.key, literal))
                            } else {
                                properties.add(mutableListOf(Pair(field.key, literal)))
                            }
                        }
                    }
            }
        }
    }
}
