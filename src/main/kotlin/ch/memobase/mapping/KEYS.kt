/*
 * mapper-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.mapping

import ch.memobase.rdf.*

/**
 * These keys define the values of the mapping file keys. Any changes in here require an update of the
 * related documentation.
 */
object KEYS {
    val validRecordTypeValues = listOf("Film", "Foto", "Radio", "Ton", "Tonbildschau", "TV", "Video")

    const val uri = "uri"
    const val type = "type"

    // Top level keys defined for the mapping structure of each object type.
    const val record = "record"
    const val physical = "physical"
    const val digital = "digital"
    const val thumbnail = "thumbnail"

    // Field annotation keys

    const val constantAnnotation = "const"
    const val prefixAnnotation = "prefix"
    const val prefixAnnotationField = "field"
    const val prefixAnnotationValue = "value"

    // Literal Mappings on Record (mostly)

    const val name = "name"
    const val descriptiveNote = "descriptiveNote"
    const val scopeAndContent = "scopeAndContent"
    const val abstract = "abstract"
    const val source = "source"
    const val title = "title"
    const val isSponsoredByMemoriav = "isSponsoredByMemoriav"
    const val hasFindingAid = "hasFindingAid"
    const val sameAs = "sameAs"
    const val relation = "relation"
    const val conditionsOfUse = "conditionsOfUse"
    const val conditionsOfAccess = "conditionsOfAccess"
    const val proxyType = "proxy"
    const val defaultProxyType = "proxydirect"
    private const val coordinates = "coordinates"

    // Rules

    const val rights = "rights"

    // Rule types

    const val holder = "holder"
    const val access = "access"
    const val usage = "usage"

    val usageSubPropertiesList = listOf(name, sameAs)

    // Places

    const val placeOfCapture = "placeOfCapture"
    const val relatedPlaces = "relatedPlaces"

    val allowedPlaceProperties = listOf(name, sameAs, coordinates)

    // Agents

    const val creators = "creators"
    private const val creator = "creator"
    const val contributors = "contributors"
    private const val contributor = "contributor"
    const val relationName = "relationName"
    private const val hasVariantNameOfAgent = "hasVariantNameOfAgent"
    private const val history = "history"
    private const val gender = "gender"
    private const val hasProfessionOrOccupation = "hasProfessionOrOccupation"
    private const val isMemberOf = "isMemberOf"

    val relationTypeMap = mapOf(
        Pair(creators, creator),
        Pair(contributors, contributor)
    )

    const val producer = "producers"
    const val relatedAgents = "relatedAgents"
    const val publishedBy = "publishedBy"

    val agentPropertiesMap = mapOf(
        Pair(producer, RDA.hasProducer),
        Pair(relatedAgents, RICO.hasOrHadSubject),
        Pair(publishedBy, RICO.hasPublisher)
    )

    // Agent Types
    private const val agent = "agent"
    private const val corporateBody = "corporateBody"
    const val person = "person"

    val agentTypeMap = mapOf(
        Pair(agent, RICO.Agent),
        Pair(corporateBody, RICO.CorporateBody),
        Pair(person, RICO.Person)
    )
    // skos:Concept

    const val subject = "subject"
    const val genre = "genre"

    // Dates

    const val creationDate = "creationDate"
    const val issuedDate = "issuedDate"
    const val temporal = "temporal"
    private const val hasBirthDate = "hasBirthDate"
    private const val hasDeathDate = "hasDeathDate"
    private const val hasPeriodOfActivityOfAgent = "hasPeriodOfActivityOfAgent"

    val agentDateProperties = listOf(hasBirthDate, hasDeathDate, hasPeriodOfActivityOfAgent)
    val allowedAgentProperties = listOf(
        relationName, name, sameAs, hasVariantNameOfAgent, history,
        descriptiveNote, gender, hasProfessionOrOccupation, isMemberOf,
        hasBirthDate, hasDeathDate, hasPeriodOfActivityOfAgent
    )
    // rico:Concepts

    const val titles = "titles"
    const val identifiers = "identifiers"
    const val languages = "languages"

    private val identifierTypes = listOf(
        RICO.Types.Identifier.original,
        RICO.Types.Identifier.callNumber,
        RICO.Types.Identifier.main,
        RICO.Types.Identifier.oldMemobase
    )
    private val titleTypes = listOf(RICO.Types.Title.main, RICO.Types.Title.series, RICO.Types.Title.broadcast)
    private val languageTypes = listOf(RICO.Types.Language.content, RICO.Types.Language.caption)

    val ricoConceptCategoryToTypesMap = mapOf(
        Pair(titles, titleTypes),
        Pair(identifiers, identifierTypes),
        Pair(languages, languageTypes)
    )

    // Physical Object Keys

    const val medium = "medium"
    const val physicalCharacteristics = "physicalCharacteristics"
    const val colour = "colour"
    const val duration = "duration"

    // removed
    private const val displayAspectRatio = "displayAspectRatio"
    private const val audioTrackConfiguration = "audioTrackConfiguration"
    private const val playbackSpeed = "playbackSpeed"
    private const val hasStandard = "hasStandard"

    // Carrier Type
    const val carrierType = "carrierType"

    // Digital Object Keys

    const val locator = "locator"

    // datatype properties

    val keysToPropertyMap = mapOf(
        Pair(abstract, DC.abstract),
        Pair(proxyType, MB.proxyType),
        Pair(relatedPlaces, DC.spatial),
        Pair(name, RICO.name),
        Pair(title, RICO.title),
        Pair(source, RICO.source),
        Pair(descriptiveNote, RICO.descriptiveNote),
        Pair(scopeAndContent, RICO.scopeAndContent),
        Pair(isSponsoredByMemoriav, RDA.hasSponsoringAgentOfResource),
        Pair(hasFindingAid, RDA.hasFindingAid),
        Pair(creationDate, DC.created),
        Pair(issuedDate, DC.issued),
        Pair(temporal, DC.temporal),
        Pair(medium, EBUCORE.hasMedium),
        Pair(colour, RDA.hasColourContent),
        Pair(physicalCharacteristics, RICO.physicalCharacteristics),
        Pair(duration, EBUCORE.duration),
        Pair(displayAspectRatio, EBUCORE.displayAspectRatio),
        Pair(audioTrackConfiguration, EBUCORE.audioTrackConfiguration),
        Pair(playbackSpeed, EBUCORE.playbackSpeed),
        Pair(hasStandard, EBUCORE.hasStandard),
        Pair(locator, EBUCORE.locator),
        Pair(sameAs, SCHEMA.sameAs),
        Pair(relation, DC.relation),
        Pair(conditionsOfUse, RICO.conditionsOfUse),
        Pair(conditionsOfAccess, RICO.conditionsOfAccess),
        Pair(isMemberOf, RDA.isMemberOf),
        Pair(hasProfessionOrOccupation, RDA.hasProfessionOrOccupation),
        Pair(hasVariantNameOfAgent, RDA.hasVariantNameOfAgent),
        Pair(gender, FOAF.gender),
        Pair(history, RICO.history),
        Pair(coordinates, WD.coordinates),
        Pair(hasBirthDate, RICO.hasBirthDate),
        Pair(hasDeathDate, RICO.hasDeathDate),
        Pair(hasPeriodOfActivityOfAgent, RDA.hasPeriodOfActivityOfAgent),
    )

    val keysToInversePropertyMap = mapOf(
        Pair(hasBirthDate, RICO.isBirthDateOf),
        Pair(hasDeathDate, RICO.isDeathDateOf),
        Pair(hasPeriodOfActivityOfAgent, RDA.isPeriodOfActivityOfAgentOf)
    )
}
