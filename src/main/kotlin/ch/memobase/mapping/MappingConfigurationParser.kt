/*
 * mapper-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.mapping

import ch.memobase.exceptions.InvalidMappingException
import ch.memobase.mapping.fields.ConstantField
import ch.memobase.mapping.fields.DirectMapField
import ch.memobase.mapping.fields.FieldParsers
import ch.memobase.mapping.fields.SimpleAnnotationField
import ch.memobase.mapping.mappers.AbstractFieldMapper
import ch.memobase.mapping.mappers.ConstantFieldMapper
import ch.memobase.mapping.mappers.DateFieldMapper
import ch.memobase.mapping.mappers.DirectFieldMapper
import ch.memobase.settings.MissingSettingException

@Suppress("UNCHECKED_CAST")
class MappingConfigurationParser {
    private var hasProxyType: Boolean = false
    private val recordFieldMappers = mutableListOf<AbstractFieldMapper>()
    private val physicalObjectFieldMappers = mutableListOf<AbstractFieldMapper>()
    private val digitalObjectFieldMappers = mutableListOf<AbstractFieldMapper>()
    private val thumbnailObjectFieldMappers = mutableListOf<AbstractFieldMapper>()

    @Throws(InvalidMappingException::class, MissingSettingException::class)
    fun parse(data: ByteArray): MapperConfiguration {
        if (data.isEmpty()) {
            throw InvalidMappingException("The provided file is empty and cannot be parsed.")
        }

        val content = Yaml.load(data)
        if (content.keys.any { it !in listOf(KEYS.record, KEYS.physical, KEYS.digital, KEYS.thumbnail) })
            throw InvalidMappingException(
                "The top level key must be one of " +
                        "'${KEYS.record}', '${KEYS.physical}', '${KEYS.digital}', '${KEYS.thumbnail}', " +
                        "but found: ${content.keys}."
            )

        return if (content.containsKey(KEYS.record)) {
            val record = try {
                content[KEYS.record] as Map<String, Any>
            } catch (ex: ClassCastException) {
                throw InvalidMappingException("The field '${KEYS.record}' must contain map as value.")
            }

            parseRecordConfig(record)

            if (content.containsKey(KEYS.physical)) {
                try {
                    parsePhysicalInstantiationConfig(content[KEYS.physical] as Map<String, Any>)
                } catch (ex: ClassCastException) {
                    throw InvalidMappingException("The field '${KEYS.physical}' must contain at least one field.")
                }
            }

            if (content.containsKey(KEYS.digital)) {
                try {
                    parseDigitalInstantiationConfig(content[KEYS.digital] as Map<String, Any>)
                } catch (ex: ClassCastException) {
                    throw InvalidMappingException("The field '${KEYS.digital}' must contain at least one field.")
                }
            }

            if (content.containsKey(KEYS.thumbnail)) {
                try {
                    parseThumbnailInstantiationConfig(content[KEYS.thumbnail] as Map<String, Any>)
                } catch (ex: ClassCastException) {
                    throw InvalidMappingException("The field '${KEYS.thumbnail}' must contain at least one field.")
                }
            }

            MapperConfiguration(
                uri = getRecordUriField(record),
                sponsoredByMemoriav = getSponsoredByMemoriavValue(record),
                hasProxyType = hasProxyType, // is set in the digital object mapping parser if applicable.
                recordType = getRecordType(record),
                recordFieldMappers = recordFieldMappers,
                physicalFieldMappers = physicalObjectFieldMappers,
                digitalFieldMappers = digitalObjectFieldMappers,
                thumbnailFieldMappers = thumbnailObjectFieldMappers
            )
        } else {
            throw InvalidMappingException("The top level field 'record' is required.")
        }
    }

    private fun getRecordUriField(values: Map<String, Any?>): String {
        return if (values.containsKey(KEYS.uri)) {
            when (val value = values[KEYS.uri]) {
                is String -> value
                else -> throw InvalidMappingException("The field 'uri' requires a field name (string) as value.")
            }
        } else {
            throw InvalidMappingException("The field 'uri' is required in the record definition.")
        }
    }

    private fun getRecordType(values: Map<String, Any?>): SimpleAnnotationField {
        return if (values.containsKey(KEYS.type)) {
            when (val value = values[KEYS.type]) {
                is Any -> FieldParsers.parseSimpleAnnotationField(KEYS.type, value)
                else -> throw InvalidMappingException("The field 'type' must contain a value.")
            }
        } else {
            throw InvalidMappingException("The field 'type' is required in the record definition.")
        }
    }

    private fun getSponsoredByMemoriavValue(values: Map<String, Any?>): Boolean {
        return if (values.containsKey(KEYS.isSponsoredByMemoriav)) {
            when (val value = values[KEYS.isSponsoredByMemoriav]) {
                is Boolean -> value
                else -> throw InvalidMappingException("The field 'isSponsoredByMemoriav' requires a boolean value.")
            }
        } else {
            false
        }
    }

    @Throws(InvalidMappingException::class)
    private fun parseRecordConfig(source: Map<String, Any?>) {
        for (entry in source) {
            val key = entry.key
            if (entry.value == null) {
                throw InvalidMappingException("The value for key '$key' on record is null, which is not allowed.")
            }
            when (key) {
                KEYS.uri, KEYS.type, KEYS.isSponsoredByMemoriav -> {
                    // ignore these values as they are processed separately.
                }
                // literal properties
                KEYS.name, KEYS.title, KEYS.descriptiveNote, KEYS.scopeAndContent, KEYS.sameAs,
                KEYS.abstract, KEYS.source, KEYS.hasFindingAid, KEYS.relation,
                KEYS.conditionsOfUse, KEYS.conditionsOfAccess ->
                    recordFieldMappers.add(MapperParsers.buildAnnotationMappers(key, entry))

                KEYS.rights ->
                    recordFieldMappers.addAll(MapperParsers.buildRuleMappers(entry.value))

                KEYS.titles, KEYS.identifiers, KEYS.languages ->
                    recordFieldMappers.addAll(MapperParsers.buildRicoConceptMappers(key, entry.value))

                KEYS.subject, KEYS.genre ->
                    recordFieldMappers.addAll(MapperParsers.buildSkosConceptMappers(key, entry.value))

                KEYS.placeOfCapture, KEYS.relatedPlaces ->
                    recordFieldMappers.addAll(MapperParsers.buildPlaceMapper(key, entry.value))

                KEYS.creationDate, KEYS.issuedDate, KEYS.temporal ->
                    when (val value = FieldParsers.parseAnnotationField(key, entry)) {
                        is DirectMapField -> recordFieldMappers.add(
                            DateFieldMapper(value)
                        )

                        else -> throw InvalidMappingException("Dates only allow simple field assignment. Invalid mapping ${entry.key}.")
                    }

                KEYS.producer, KEYS.relatedAgents, KEYS.creators, KEYS.contributors, KEYS.publishedBy ->
                    recordFieldMappers.addAll(MapperParsers.buildAgentMapper(key, entry.value))

                else -> throw InvalidMappingException("Unknown key '$key' in record mapping.")
            }
        }
    }

    private fun parsePhysicalInstantiationConfig(source: Map<String, Any?>) {
        for (entry in source) {
            val key = entry.key
            if (entry.value == null) {
                throw InvalidMappingException("The value for key '$key' on physical is null, which is not allowed.")
            }
            when (key) {
                KEYS.descriptiveNote, KEYS.medium, KEYS.physicalCharacteristics,
                KEYS.colour, KEYS.duration,
                KEYS.conditionsOfUse, KEYS.conditionsOfAccess ->
                    physicalObjectFieldMappers.add(MapperParsers.buildAnnotationMappers(key, entry))

                KEYS.identifiers ->
                    physicalObjectFieldMappers.addAll(MapperParsers.buildRicoConceptMappers(key, entry.value))

                KEYS.rights ->
                    physicalObjectFieldMappers.addAll(MapperParsers.buildRuleMappers(entry.value))

                KEYS.carrierType ->
                    physicalObjectFieldMappers.add(MapperParsers.buildCarrierTypeMapper(key, entry))

                else -> throw InvalidMappingException("Unknown key '$key' in physical object mapping.")
            }
        }
    }

    private fun parseDigitalInstantiationConfig(source: Map<String, Any?>) {
        for (entry in source) {
            val key = entry.key
            if (entry.value == null) {
                throw InvalidMappingException("The value for key '$key' on digital is null, which is not allowed.")
            }
            when (key) {
                // checks if the proxy type is defined locally.
                KEYS.proxyType -> {
                    when(val value = FieldParsers.parseAnnotationField(key, entry)) {
                        is DirectMapField -> {
                            digitalObjectFieldMappers.add(DirectFieldMapper(value, "proxydirect"))
                        }
                        is ConstantField -> {
                            digitalObjectFieldMappers.add(ConstantFieldMapper(value))
                        }
                        else -> throw InvalidMappingException("The value for key '$key' on digital must be a string.")
                    }
                    hasProxyType = true
                }

                KEYS.locator, KEYS.descriptiveNote, KEYS.duration, KEYS.conditionsOfUse, KEYS.conditionsOfAccess ->
                    digitalObjectFieldMappers.add(MapperParsers.buildAnnotationMappers(key, entry))

                KEYS.identifiers ->
                    digitalObjectFieldMappers.addAll(MapperParsers.buildRicoConceptMappers(key, entry.value))

                KEYS.rights ->
                    digitalObjectFieldMappers.addAll(MapperParsers.buildRuleMappers(entry.value))

                else -> throw InvalidMappingException("Unknown key '$key' in digital object mapping.")
            }
        }
    }

    private fun parseThumbnailInstantiationConfig(source: Map<String, Any?>) {
        for (entry in source) {
            val key = entry.key
            if (entry.value == null) {
                throw InvalidMappingException("The value for key '$key' on thumbnail is null, which is not allowed.")
            }
            when (key) {
                KEYS.locator, KEYS.descriptiveNote, KEYS.duration, KEYS.conditionsOfUse, KEYS.conditionsOfAccess ->
                    thumbnailObjectFieldMappers.add(MapperParsers.buildAnnotationMappers(key, entry))

                KEYS.identifiers ->
                    thumbnailObjectFieldMappers.addAll(MapperParsers.buildRicoConceptMappers(key, entry.value))

                KEYS.rights ->
                    thumbnailObjectFieldMappers.addAll(MapperParsers.buildRuleMappers(entry.value))

                else -> throw InvalidMappingException("Unknown key '$key' in thumbnail mapping.")
            }
        }
    }
}
