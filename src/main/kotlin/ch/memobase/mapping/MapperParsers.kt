/*
 * mapper service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.mapping

import ch.memobase.exceptions.InvalidMappingException
import ch.memobase.mapping.fields.ConfigField
import ch.memobase.mapping.fields.ConstantField
import ch.memobase.mapping.fields.DirectMapField
import ch.memobase.mapping.fields.FieldParsers
import ch.memobase.mapping.fields.LanguageField
import ch.memobase.mapping.fields.ListField
import ch.memobase.mapping.fields.PrefixField
import ch.memobase.mapping.mappers.AbstractFieldMapper
import ch.memobase.mapping.mappers.AgentFieldMapper
import ch.memobase.mapping.mappers.CarrierTypeMapper
import ch.memobase.mapping.mappers.ConstantFieldMapper
import ch.memobase.mapping.mappers.DirectFieldMapper
import ch.memobase.mapping.mappers.ExpandedRuleFieldMapper
import ch.memobase.mapping.mappers.LanguageFieldMapper
import ch.memobase.mapping.mappers.ListFieldMapper
import ch.memobase.mapping.mappers.PlaceFieldMapper
import ch.memobase.mapping.mappers.PrefixFieldMapper
import ch.memobase.mapping.mappers.RicoConceptMapper
import ch.memobase.mapping.mappers.RuleFieldMapper
import ch.memobase.mapping.mappers.SkosConceptFieldMapper
import ch.memobase.mapping.mappers.TypeFieldMapper
import ch.memobase.rdf.SKOS
import kotlin.reflect.full.createInstance

@Suppress("UNCHECKED_CAST")
object MapperParsers {

    fun buildCarrierTypeMapper(parent: String, value: Map.Entry<String, Any?>): AbstractFieldMapper {
        return CarrierTypeMapper(FieldParsers.parseAnnotationField(parent, value))
    }

    fun buildAnnotationMappers(parent: String, value: Any?): AbstractFieldMapper {
        return when (val field = FieldParsers.parseAnnotationField(parent, value as Map.Entry<String, Any>)) {
            is DirectMapField -> DirectFieldMapper(field)
            is ConstantField -> ConstantFieldMapper(field)
            is ListField -> ListFieldMapper(field)
            is LanguageField -> LanguageFieldMapper(field)
            is PrefixField -> PrefixFieldMapper(field)
        }
    }

    fun buildRicoConceptMappers(key: String, value: Any?): List<AbstractFieldMapper> {
        return when (value) {
            is Map<*, *> -> {
                extractRicoConceptFields(key, value as Map<String, Any>)
            }
            is List<*> -> {
                value.flatMap {
                    if (it is Map<*, *>) {
                        extractRicoConceptFields(key, it as Map<String, Any>)
                    } else {
                        throw InvalidMappingException("Expected a key-value map in section $key.")
                    }
                }
            }
            else -> throw InvalidMappingException("Expected a key-value map in section $key.")
        }
    }

    private fun extractRicoConceptFields(key: String, map: Map<String, Any>): List<AbstractFieldMapper> {
        return map.entries.map { entry ->
            KEYS.ricoConceptCategoryToTypesMap[key].let { typeList ->
                if (typeList != null)
                    FieldParsers.parseFieldWithKeyValidation(key, entry, typeList)
                else
                    throw InvalidMappingException("Could not find a types configuration for field $key.")
            }
        }.map { field ->
            RicoConceptMapper(key, field)
        }
    }

    fun buildAgentMapper(key: String, value: Any?): List<AbstractFieldMapper> {
        return when (value) {
            is Map<*, *> -> {
                extractAgentTypeMapper(key, value as Map<String, Any>)
            }
            is List<*> -> {
                value.flatMap {
                    extractAgentTypeMapper(key, it as Map<String, Any>)
                }
            }
            else -> throw InvalidMappingException("The section ${KEYS.creators} expects a map or list.")
        }
    }

    fun buildRuleMappers(value: Any?): List<AbstractFieldMapper> {
        when (value) {
            is Map<*, *> -> {
                return value.entries.map {
                    when (it.key) {
                        KEYS.usage ->
                            ExpandedRuleFieldMapper(
                                    it.key as String,
                                    FieldParsers.parseSubFieldProperties(
                                            it.key as String,
                                            (it.value as Map<String, Any>).entries,
                                            KEYS.usageSubPropertiesList
                                    )
                            )
                        KEYS.access ->
                            return@map RuleFieldMapper(FieldParsers.parseAnnotationField(KEYS.access, it as Map.Entry<String, Any>))
                        KEYS.holder ->
                            return@map RuleFieldMapper(FieldParsers.parseAnnotationField(KEYS.holder, it as Map.Entry<String, Any>))
                        else -> throw InvalidMappingException("Rights section has a fixed set of valid types: ${KEYS.rights}.")
                    }
                }
            }
            else -> throw InvalidMappingException("Expected key value map under rights label.")
        }
    }

    fun buildSkosConceptMappers(key: String, value: Any?): List<AbstractFieldMapper> {
        return when (value) {
            is Map<*, *> -> {
                listOf(
                        extractEntityFields<SkosConceptFieldMapper>(
                                key,
                                value as Map<String, Any>,
                                SKOS.acceptedPropertiesList
                        )
                )
            }
            is List<*> -> {
                value.map {
                    try {
                        extractEntityFields<SkosConceptFieldMapper>(
                                key,
                                it as Map<String, Any>,
                                SKOS.acceptedPropertiesList

                        )
                    } catch (ex: ClassCastException) {
                        throw InvalidMappingException("Expected a map inside of the list entry in the $key section.")
                    }
                }
            }
            else -> throw InvalidMappingException("Expected a mapping or a list inside of the $key section!")
        }
    }

    private fun extractAgentTypeMapper(key: String, value: Map<String, Any>): List<AbstractFieldMapper> {
        return value.entries.map {
            extractEntityFields<AgentFieldMapper>(it.key, it.value as Map<String, Any>, KEYS.allowedAgentProperties)
        }.map {
            it as AgentFieldMapper
            it.sourceKey = key
            it
        }
    }

    fun buildPlaceMapper(key: String, value: Any?): List<AbstractFieldMapper> {
        return when (value) {
            is Map<*, *> -> {
                listOf(
                        extractEntityFields<PlaceFieldMapper>(
                                key, value as Map<String, Any>, KEYS.allowedPlaceProperties
                        )
                )
            }
            is List<*> -> {
                return value.map {
                    try {
                        extractEntityFields<PlaceFieldMapper>(
                                key, it as Map<String, Any>, KEYS.allowedPlaceProperties
                        )
                    } catch (ex: ClassCastException) {
                        throw InvalidMappingException("Expected a map inside of the list entry in the $key section.")
                    }
                }
            }
            else -> throw InvalidMappingException("Expected a mapping or a list inside of the $key section!")
        }
    }

    private inline fun <reified T : TypeFieldMapper> extractEntityFields(
        key: String,
        value: Map<String, Any>,
        validationList: List<String>
    ): TypeFieldMapper {
        val configFields = mutableListOf<ConfigField>()
        for (entry in value.entries) {
            configFields.add(
                    FieldParsers.parseFieldWithKeyValidation(
                            key,
                            entry,
                            validationList
                    )
            )
        }
        val instance = T::class.createInstance() as TypeFieldMapper
        instance.setFields(key, configFields)
        return instance
    }
}
