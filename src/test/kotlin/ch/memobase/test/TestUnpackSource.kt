/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.test

import ch.memobase.mapping.fields.FieldParsers.unpackSource
import ch.memobase.mapping.fields.SourceElement
import com.beust.klaxon.Klaxon
import com.beust.klaxon.json
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.io.File
import java.io.FileInputStream

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestUnpackSource {

    private val klaxon = Klaxon()
    private val basePath = "src/test/resources/unpack_source"

    private fun read(test: String): Map<String, Any> {
        return klaxon.parse<Map<String, Any>>(FileInputStream(File("$basePath/$test/input.json")))!!
    }

    @Test
    fun `test string`() {
        val output = unpackSource("agent", "name", read("string"))
        assertThat(output)
            .isEqualTo(SourceElement.SimpleString("Hello World"))
    }

    @Test
    fun `test list`() {
        val output = unpackSource("agent", "name", read("list"))
        assertThat(output)
            .isEqualTo(SourceElement.StringList(json { array("Herbert", "Hansueli") } as List<String>))
    }

    @Test
    fun `test object string`() {
        val output = unpackSource("agent", "name.firstName", read("object_string"))
        assertThat(output)
            .isEqualTo(SourceElement.SimpleString("Herbert"))
    }

    @Test
    fun `test object list`() {
        val output = unpackSource("agent", "name.firstName", read("object_list"))
        assertThat(output)
            .isEqualTo(SourceElement.StringList(json { array("Herbert", "Hansueli") } as List<String>))
    }

    @Test
    fun `test object list string`() {
        val output = unpackSource("agent", "name.firstName", read("object_list_string"))
        assertThat(output)
            .isEqualTo(SourceElement.StringList(listOf("Herbert", "Angela")))
    }

    @Test
    fun `test object list list`() {
        val output = unpackSource("agent", "name.firstName", read("object_list_list"))
        assertThat(output)
            .isEqualTo(SourceElement.StringList(listOf("Herbert", "Hansuale", "Angela", "Barbara")))
    }
}