/*
 * mapper-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.test

import ch.memobase.exceptions.InvalidMappingException
import ch.memobase.mapping.MapperParsers
import ch.memobase.mapping.mappers.AgentFieldMapper
import java.util.function.Consumer
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertThrows

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestMapperParseHelpers {

    @Test
    fun `test extractAgentTypeMapper`() {
        val mappers = MapperParsers.buildAgentMapper(
            "creators",
            listOf(
                mapOf(
                    Pair(
                        "person", mapOf(
                            Pair("name", "creatorPerson.name"),
                            Pair("relationName", "creatorPerson.relationName")
                        )
                    )
                )
            )
        )
        val mapperReqs: Consumer<AgentFieldMapper> = Consumer { agentFieldMapper: AgentFieldMapper ->
            assertThat(agentFieldMapper.sourceKey).isEqualTo("creators")
            assertThat(agentFieldMapper.agentClassType).isEqualTo("person")
        }
        val mapper = mappers[0] as AgentFieldMapper
        assertThat(mapper)
            .satisfies(mapperReqs)
    }

    // TODO: Test that there is nothing else.
    @Test
    fun `test extractAgentTypeMapper without relation`() {
        val mappers = MapperParsers.buildAgentMapper(
            "creators",
            listOf(
                mapOf(
                    Pair(
                        "person", mapOf(
                            Pair("name", "creatorPerson.name")
                        )
                    )
                )
            )
        )
        val mapperReqs: Consumer<AgentFieldMapper> = Consumer { agentFieldMapper: AgentFieldMapper ->
            assertThat(agentFieldMapper.sourceKey).isEqualTo("creators")
            assertThat(agentFieldMapper.agentClassType).isEqualTo("person")
        }
        val mapper = mappers[0] as AgentFieldMapper
        assertThat(mapper)
            .satisfies(mapperReqs)
    }

    // TODO: This should fail!
    @Test
    fun `test extractAgentTypeMapper empty`() {
        val mappers = MapperParsers.buildAgentMapper(
            "creators",
            listOf(
                mapOf(
                    Pair(
                        "person", emptyMap<String, Any>()
                    )
                )
            )
        )
        val mapperReqs: Consumer<AgentFieldMapper> = Consumer { agentFieldMapper: AgentFieldMapper ->
            assertThat(agentFieldMapper.sourceKey).isEqualTo("creators")
            assertThat(agentFieldMapper.agentClassType).isEqualTo("person")
        }
        val mapper = mappers[0] as AgentFieldMapper
        assertThat(mapper)
            .satisfies(mapperReqs)
    }

    @Test
    fun `test extractAgentTypeMapper with different property`() {
        assertThrows<InvalidMappingException> {
            MapperParsers.buildAgentMapper(
                "creators",
                listOf(
                    mapOf(
                        Pair(
                            "person", mapOf(
                                Pair("people", "creatorPerson.name")
                            )
                        )
                    )
                )
            )
        }
    }
}
