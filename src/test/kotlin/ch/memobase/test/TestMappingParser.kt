/*
 * mapper-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.test

import ch.memobase.exceptions.InvalidMappingException
import ch.memobase.mapping.MapperConfiguration
import ch.memobase.mapping.MappingConfigurationParser
import ch.memobase.mapping.fields.FieldParsers
import com.beust.klaxon.Klaxon
import org.apache.jena.vocabulary.LocationMappingVocab.mapping
import java.io.File
import java.io.FileInputStream
import java.nio.charset.Charset
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertThrows

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestMappingParser {

    private val path = "src/test/resources"
    private fun readFile(fileName: String): String {
        return FileInputStream("$path/output/$fileName.json").readBytes().toString(Charset.defaultCharset())
    }

    private fun mapping(fileName: String): ByteArray {
        return File("$path/mapperParser/$fileName.yml").readBytes()
    }

    private val klaxon = Klaxon()

    @Test
    fun `test 1 minimal mapping parser`() {
        val mappingParser = MappingConfigurationParser()
        val output = mappingParser.parse(File("$path/mapping1.yml").readBytes())
        assertThat(output)
            .isEqualTo(
                MapperConfiguration(
                    "TestField",
                    sponsoredByMemoriav = false,
                    hasProxyType = false,
                    recordType = FieldParsers.parseSimpleAnnotationField("type", mapOf(Pair("const", "Foto"))),
                    recordFieldMappers = emptyList(),
                    physicalFieldMappers = emptyList(),
                    digitalFieldMappers = emptyList(),
                    thumbnailFieldMappers = emptyList()
                )
            )
    }

    @Test
    fun `test 2 sponsor field parser`() {
        val mappingParser = MappingConfigurationParser()
        val output = mappingParser.parse(File("$path/mapping2.yml").readBytes())
        assertThat(output)
            .isEqualTo(
                MapperConfiguration(
                    "TestField",
                    sponsoredByMemoriav = true,
                    hasProxyType = false,
                    recordType = FieldParsers.parseSimpleAnnotationField("type", mapOf(Pair("const", "Foto"))),
                    recordFieldMappers = emptyList(),
                    physicalFieldMappers = emptyList(),
                    digitalFieldMappers = emptyList(),
                    thumbnailFieldMappers = emptyList()
                )
            )
    }

    @Test
    fun `test 3 record parser`() {
        val mappingParser = MappingConfigurationParser()
        val string = klaxon.toJsonString(mappingParser.parse(File("$path/mapping3.yml").readBytes()))
        assertThat(string)
            .isEqualTo(readFile("output3"))
    }

    @Test
    fun `test 4 skos field parser`() {
        val mappingParser = MappingConfigurationParser()
        val string = klaxon.toJsonString(mappingParser.parse(File("$path/mapping4.yml").readBytes()))
        assertThat(string)
            .isEqualTo(readFile("output4"))
    }

    @Test
    fun `test 5 proxy field on digital object`() {
        val mappingParser = MappingConfigurationParser()
        val string = klaxon.toJsonString(mappingParser.parse(File("$path/mapping5.yml").readBytes()))
        assertThat(string)
            .isEqualTo(readFile("output5"))
    }

    @Test
    fun `test 6 list of const for translations`() {
        val mappingParser = MappingConfigurationParser()
        val string = klaxon.toJsonString(mappingParser.parse(File("$path/mapping6.yml").readBytes()))
        assertThat(string)
            .isEqualTo(readFile("output6"))
    }

    @Test
    fun `test 7 no type value error message`() {
        val mappingParser = MappingConfigurationParser()
        val exception = assertThrows<InvalidMappingException> {
            mappingParser.parse(mapping("mappingNoTypeValue"))
        }
        assertThat(exception.message)
            .isEqualTo("The value for key 'type' on record is null, which is not allowed.")
    }

    @Test
    fun `test 8 empty file`() {
        val mappingParser = MappingConfigurationParser()
        val exception = assertThrows<InvalidMappingException> {
            mappingParser.parse(mapping("emptyFile"))
        }
        assertThat(exception.message)
            .isEqualTo("The provided file is empty and cannot be parsed.")
    }

    @Test
    fun `test 9 constant type without value`() {
        val mappingParser = MappingConfigurationParser()
        val exception = assertThrows<InvalidMappingException> {
            mappingParser.parse(mapping("emptyConstantFieldType"))
        }
        assertThat(exception.message)
            .isEqualTo("The constant annotation 'const:' requires a string as value for field type.")
    }

    @Test
    fun `test 10 empty language tag`() {
        val mappingParser = MappingConfigurationParser()
        val exception = assertThrows<InvalidMappingException> {
            mappingParser.parse(mapping("emptyLanguageTag"))
        }
        assertThat(exception.message)
            .isEqualTo("The language code de field must have a value for field 'titles'.")
    }

    @Test
    fun `test 11 empty property`() {
        val mappingParser = MappingConfigurationParser()
        val exception = assertThrows<InvalidMappingException> {
            mappingParser.parse(mapping("emptyKeyOnRecord"))
        }
        assertThat(exception.message)
            .isEqualTo("The value for key 'source' on record is null, which is not allowed.")
    }

    @Test
    fun `test 12 empty subfield on record`() {
        val mappingParser = MappingConfigurationParser()
        val exception = assertThrows<InvalidMappingException> {
            mappingParser.parse(mapping("emptySubfieldOnRecord"))
        }
        assertThat(exception.message)
            .isEqualTo("The field languages contains a key with a null value: caption.")
    }

    @Test
    @Disabled
    fun `test XX example from system`() {
        val mappingParser = MappingConfigurationParser()

        mappingParser.parse(mapping("test"))
        val exception = assertThrows<NullPointerException> {
        }
        assertThat(exception.message)
            .isEqualTo("")
    }
}
