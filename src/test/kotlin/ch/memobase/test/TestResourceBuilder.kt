/*
 * mapper-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.test

import ch.memobase.builder.ResourceBuilder
import ch.memobase.mapping.MappingConfigurationParser
import com.beust.klaxon.Klaxon
import org.apache.jena.riot.RDFFormat
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.io.File
import java.io.FileInputStream
import java.nio.charset.Charset

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestResourceBuilder {
    private val path = "src/test/resources/resource-builder"

    @Test
    fun `test exclude digital object without input`() {
        val config = MappingConfigurationParser().parse((FileInputStream(File("$path/mapping1.yml")).readBytes()))
        val resourceBuilder = ResourceBuilder(
            mapOf(

            ),
            config,
            "tst",
            "tst-001",
            false
        )
        resourceBuilder.generateDigitalObject()
            .generateThumbnailObject()
        assertThat(resourceBuilder.errorMessages)
            .isEqualTo(listOf("Removed digital object from resource as there are no properties extracted from the data."))
    }

    @Test
    fun `test include digital object with input`() {
        val config = MappingConfigurationParser().parse(FileInputStream(File("$path/mapping1.yml")).readBytes())
        val resourceBuilder = ResourceBuilder(
            mapOf(
                Pair("id", "tst-001-202"),
                Pair("locator", "https://www.example.org/image.jpg")
            ),
            config,
            "tst",
            "tst-001",
            false
        )
        resourceBuilder.generateRecord()
        resourceBuilder.generateDigitalObject()
            .generateThumbnailObject()
        assertThat(resourceBuilder.errorMessages)
            .isEqualTo(emptyList<String>())
    }

    @Test
    fun `test custom input`() {
        val config = MappingConfigurationParser().parse(File("$path/mapping2.yml").readBytes())
        val resourceBuilder = ResourceBuilder(
            Klaxon().parseJsonObject(FileInputStream(File("$path/input2.json"))
                .reader(Charset.defaultCharset())) as Map<String, Any>,
            config,
            "tst",
            "tst-001",
            false
        )
        resourceBuilder.generateRecord()
        resourceBuilder
            .generateDigitalObject()
            .generateThumbnailObject()
        assertThat(resourceBuilder.errorMessages)
            .isEqualTo(emptyList<String>())
    }

    @Test
    fun `test proxytype input`() {
        val config = MappingConfigurationParser().parse(File("$path/mapping3.yml").readBytes())
        val resourceBuilder = ResourceBuilder(
            Klaxon().parseJsonObject(FileInputStream(File("$path/input3.json"))
                .reader(Charset.defaultCharset())) as Map<String, Any>,
            config,
            "tst",
            "tst-001",
            false
        )
        resourceBuilder.generateRecord()
        resourceBuilder
            .generateDigitalObject()
            .generateThumbnailObject()
        assertThat(resourceBuilder.errorMessages)
            .isEqualTo(emptyList<String>())

        val output = resourceBuilder.writeRecord(RDFFormat.JSONLD)
        assertThat(output.second)
            .contains("proxydirect")
    }
}
