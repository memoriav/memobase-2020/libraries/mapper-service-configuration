/*
 * mapper-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.test

import ch.memobase.builder.Record
import ch.memobase.mapping.MapperParsers
import ch.memobase.rdf.RDF
import ch.memobase.rdf.RICO
import org.apache.jena.rdf.model.Resource
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestSkosConceptMapper {
    @Test
    // TODO: This test doesn't actually work anymore! it just tests as true, but the check doesn't check anything.
    fun `test skos concept mapper`() {
        val skosConceptFieldMapper = MapperParsers.buildSkosConceptMappers("subject",
                listOf(
                    mapOf(Pair("prefLabel", mapOf(Pair("de", "Schlagworte")))),
                    mapOf(Pair("editorialNote", mapOf(Pair("const", "Ursprungsfeld: Schlagworte"))))

                    )

            )[0]
        val source: Map<String, Any> = mapOf(
            Pair("Schlagworte", "Subject 1, Subject 2")
        )
        val record = Record(
            "1",
            "Foto",
            "rs1",
            "ins1",
            hasSponsoringAgent = true,
            isPublished = false
        )

        skosConceptFieldMapper.apply(source, record)

        assertThat(record.model.listSubjectsWithProperty(RDF.type, RICO.CreationRelation).toList())
            .asList()
            .allMatch {
                it as Resource
                it.hasProperty(RICO.name, "Schauspieler") && it.hasProperty(RICO.name, "DUMMY-VALUE")
            }
    }
}
