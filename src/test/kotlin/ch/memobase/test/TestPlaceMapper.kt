/*
 * mapper-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.test

import ch.memobase.builder.ResourceBuilder
import ch.memobase.mapping.MappingConfigurationParser
import ch.memobase.settings.HeaderMetadata
import com.beust.klaxon.Klaxon
import org.apache.jena.riot.RDFFormat
import org.apache.logging.log4j.LogManager
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll
import java.io.File
import java.io.FileOutputStream

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestPlaceMapper {
    private val log = LogManager.getLogger(this::class.java)

    private val klaxon = Klaxon()

    private val headerMetadata = HeaderMetadata(
        "test-001",
        "1",
        "test",
        false,
        "record",
        "identifierMain",
        0, 0, 0, 0
    )
    private val regex = Regex("(_:B[A-Za-z0-9]+)")
    private val regexTime = Regex("\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}")

    private fun sort(source: List<String>): String {
        return source.map {
            var replacedString = it
            for (matchResult in regex.findAll(it)) {
                replacedString = replacedString.replace(matchResult.groups[0]?.value.orEmpty(), "_:B")
            }
            for (matchResult in regexTime.findAll(it)) {
                replacedString = replacedString.replace(matchResult.groups[0]?.value.orEmpty(), "2020-10-10T09:10:22")
            }
            replacedString
        }.sorted().reduce { acc, s -> acc + "\n" + s }.trim()
    }

    private val resourcePath = "src/test/resources/turtleOutput"
    private val inputPath = "src/test/resources/place-mapper"
    private fun readInputFile(fileName: String): String {
        return File("$inputPath/$fileName.json").readText()
    }

    private fun readOutputFile(fileName: String): String {
        return sort(File("$inputPath/$fileName.nt").readLines())
    }

    private fun readMapping(fileName: String): ByteArray {
        return File("$inputPath/$fileName.yml").readBytes()
    }
    @Test
    fun `test place creation`() {
        val source = klaxon.parse<Map<String, Any>>(readInputFile("1-input")).orEmpty()
        val mapping = MappingConfigurationParser()
        val configuration = mapping.parse(readMapping("1-mapping"))
        val result = ResourceBuilder(
            source,
            configuration,
            headerMetadata.institutionId,
            headerMetadata.recordSetId,
            headerMetadata.isPublished
        ).extractRecordId()
            .extractRecordTypeValue()
            .generateRecord()
            .generatePhysicalObject()
            .generateDigitalObject()
            .generateThumbnailObject()
            .addDerivedConnection()

        val ntriples = result
            .writeRecord(RDFFormat.NTRIPLES_UTF8)
        val turtle = result.writeRecord(RDFFormat.TURTLE_PRETTY)
        FileOutputStream(File("$resourcePath/places-test-place-creation.ttl")).use {
            it.bufferedWriter().use { writer ->
                writer.write(turtle.second)
            }
        }
        ntriples.third.forEach {
            log.error(it)
        }
        assertAll(
            "",
            {
                Assertions.assertThat(sort(ntriples.second.split("\n")))
                    .isEqualTo(readOutputFile("1-output"))
            }
        )
    }
}
