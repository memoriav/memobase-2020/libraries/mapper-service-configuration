/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.test

import ch.memobase.builder.ResourceBuilder
import ch.memobase.mapping.MappingConfigurationParser
import com.beust.klaxon.Klaxon
import org.apache.jena.riot.RDFFormat
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.io.File


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Disabled
class TestTransformation {
    private val klaxon = Klaxon()
    private val path = "src/test/resources/transformations"

    // Unclear if we ever actually need this. We will see if multiple call numbers are ever used.
    @Test
    fun `test transformation with multiple call numbers`() {
        val folder = "$path/multipleCallNumbers"
        val parser = MappingConfigurationParser()
        val config = parser.parse(File("$folder/mapping.yml").readBytes())

        val builder = ResourceBuilder(
            source = klaxon.parse<Map<String, Any>>(File("$folder/source.json"))!!,
            config = config,
            institutionId = "tst",
            recordSetId = "tst-001",
            isPublished = true
        )

        val result = builder.extractRecordId()
            .generateRecord()
            .generatePhysicalObject()
            .writeRecord(RDFFormat.JSONLD)

        println(result)
    }
}