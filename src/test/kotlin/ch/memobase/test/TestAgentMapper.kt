/*
 * mapper-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.test

import ch.memobase.builder.Record
import ch.memobase.builder.ResourceBuilder
import ch.memobase.mapping.MapperParsers
import ch.memobase.mapping.MappingConfigurationParser
import ch.memobase.rdf.RICO
import ch.memobase.settings.HeaderMetadata
import com.beust.klaxon.Klaxon
import java.io.File
import java.io.FileOutputStream
import org.apache.jena.rdf.model.impl.SelectorImpl
import org.apache.jena.riot.Lang
import org.apache.jena.riot.RDFDataMgr
import org.apache.jena.riot.RDFFormat
import org.apache.logging.log4j.LogManager
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Condition
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll
import java.util.function.Predicate

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestAgentMapper {
    private val log = LogManager.getLogger(this::class.java)

    private val klaxon = Klaxon()

    private val headerMetadata = HeaderMetadata(
        "test-001",
        "1",
        "test",
        false,
        "record",
        "identifierMain",
        0, 0, 0, 0
    )

    private val mapper = MapperParsers.buildAgentMapper(
        "creators",
        listOf(
            mapOf(
                Pair(
                    "person", mapOf(
                        Pair("name", "creatorPerson.name"),
                        Pair("relationName", "creatorPerson.relationName")
                    )
                )
            )
        )
    )[0]

    private val regex = Regex("(_:B[A-Za-z0-9]+)")
    private val regexTime = Regex("\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}")

    private fun sort(source: List<String>): String {
        return source.map {
            var replacedString = it
            for (matchResult in regex.findAll(it)) {
                replacedString = replacedString.replace(matchResult.groups[0]?.value.orEmpty(), "_:B")
            }
            for (matchResult in regexTime.findAll(it)) {
                replacedString = replacedString.replace(matchResult.groups[0]?.value.orEmpty(), "2020-10-10T09:10:22")
            }
            replacedString
        }.sorted().reduce { acc, s -> acc + "\n" + s }.trim()
    }

    private val resourcePath = "src/test/resources/turtleOutput"
    private val inputPath = "src/test/resources/agent-mapper"
    private fun readInputFile(fileName: String): String {
        return File("$inputPath/$fileName.json").readText()
    }

    private fun readOutputFile(fileName: String): String {
        return sort(File("$inputPath/$fileName.nt").readLines())
    }

    private fun readMapping(fileName: String): ByteArray {
        return File("$inputPath/$fileName.yml").readBytes()
    }

    // TODO: Actually test this.
    @Test
    fun `test agent mapper with name only`() {
        val source: Map<String, Any> = mapOf(
            Pair("creatorPerson", mapOf(Pair("name", "Hans Zimmer")))
        )
        val record = Record(
            "1",
            "Foto",
            "rs1",
            "ins1",
            hasSponsoringAgent = true,
            isPublished = false
        )
        mapper.apply(source, record)
        RDFDataMgr.write(FileOutputStream("$resourcePath/agent-mapper-with-name-only.ttl"), record.model, Lang.TURTLE)
    }

    @Test
    fun `test agent mapper with name and relation`() {
        val source: Map<String, Any> = mapOf(
            Pair(
                "creatorPerson",
                mapOf(Pair("name", "Hans Zimmer"), Pair("relationName", "Author"))
            )
        )
        val record = Record(
            "1",
            "Foto",
            "rs1",
            "ins1",
            hasSponsoringAgent = true,
            isPublished = false
        )
        mapper.apply(source, record)
        RDFDataMgr.write(
            FileOutputStream("$resourcePath/agent-mapper-with-name-and-relation.ttl"),
            record.model,
            Lang.TURTLE
        )
    }

    @Test
    fun `test agent mapper with multiple authors`() {
        val source: Map<String, Any> = mapOf(
            Pair(
                "creatorPerson",
                listOf(
                    mapOf(Pair("name", "PERSON 1"), Pair("relationName", "RELATION 1")),
                    mapOf(Pair("name", "PERSON 2"), Pair("relationName", "RELATION 2"))
                )
            )
        )
        val record = Record(
            "1",
            "Foto",
            "rs1",
            "ins1",
            hasSponsoringAgent = true,
            isPublished = false
        )
        mapper.apply(source, record)
        RDFDataMgr.write(
            FileOutputStream("$resourcePath/agent-mapper-with-multiple-authors.ttl"),
            record.model,
            Lang.TURTLE
        )
    }

    @Test
    fun `test missing role name in list`() {
        val source: Map<String, Any> = mapOf(
            Pair(
                "creatorPerson",
                listOf(
                    mapOf(Pair("name", "First Person"), Pair("relationName", "Relation 1")),
                    mapOf(Pair("name", "Second Person")),
                    mapOf(Pair("name", "Third Person"), Pair("relationName", "Relation 3"))
                )
            )
        )
        val record = Record(
            "1",
            "Foto",
            "rs1",
            "ins1",
            hasSponsoringAgent = true,
            isPublished = false
        )
        mapper.apply(source, record)
        val selectR3 = SelectorImpl(null, RICO.name, "Relation 3")
        val selectP3 = SelectorImpl(null, RICO.name, "Third Person")
        val relationThreeStatements = record.model.listStatements(selectR3).toList()
        val personThreeStatements = record.model.listStatements(selectP3).toList()
        val selectCombination = SelectorImpl(
            relationThreeStatements[0].subject,
            RICO.creationRelationHasTarget,
            personThreeStatements[0].subject
        )
        val relationIsConnectedToPerson = record.model.listStatements(selectCombination).toList()

        assertAll(
            "",
            {
                assertThat(relationThreeStatements)
                    .hasSize(1)
            },
            {
                assertThat(personThreeStatements)
                    .hasSize(1)
            },
            {
                assertThat(relationIsConnectedToPerson)
                    .hasSize(1)
            }
        )
        RDFDataMgr.write(
            FileOutputStream("$resourcePath/missing-relation-name-value-in-list.ttl"),
            record.model,
            Lang.TURTLE
        )
    }

    @Test
    fun `test multiple contributor missing name`() {
        val source = klaxon.parse<Map<String, Any>>(readInputFile("input-1")).orEmpty()
        val mapping = MappingConfigurationParser()
        val configuration = mapping.parse(readMapping("mapping-1"))
        val result = ResourceBuilder(
            source,
            configuration,
            headerMetadata.institutionId,
            headerMetadata.recordSetId,
            headerMetadata.isPublished
        ).extractRecordId()
            .extractRecordTypeValue()
            .generateRecord()
            .generatePhysicalObject()
            .generateDigitalObject()
            .generateThumbnailObject()
            .addDerivedConnection()

        val ntriples = result
            .writeRecord(RDFFormat.NTRIPLES_UTF8)
        val turtle = result.writeRecord(RDFFormat.TURTLE_PRETTY)
        FileOutputStream(File("$resourcePath/output-multiple-contributors.ttl")).use {
            it.bufferedWriter().use { writer ->
                writer.write(turtle.second)
            }
        }
        ntriples.third.forEach {
            log.error(it)
        }
        assertAll(
            "",
            {
                assertThat(sort(ntriples.second.split("\n")))
                    .isEqualTo(readOutputFile("output-1"))
            },
            { assertThat(ntriples.third[0]).isEqualTo("Could not apply AgentFieldMapper for record for field 'name', because: class ch.memobase.exceptions.InvalidInputException: Encountered an unexpected empty value for mapping name -> contributorPerson.name..") }
        )

    }

    @Test
    fun `test additional text fields`() {
        val source = klaxon.parse<Map<String, Any>>(readInputFile("input-2")).orEmpty()
        val mapping = MappingConfigurationParser()
        val configuration = mapping.parse(readMapping("mapping-2"))
        val result = ResourceBuilder(
            source,
            configuration,
            headerMetadata.institutionId,
            headerMetadata.recordSetId,
            headerMetadata.isPublished
        ).extractRecordId()
            .extractRecordTypeValue()
            .generateRecord()
            .generatePhysicalObject()
            .generateDigitalObject()
            .generateThumbnailObject()
            .addDerivedConnection()

        val ntriples = result
            .writeRecord(RDFFormat.NTRIPLES_UTF8)
        val turtle = result.writeRecord(RDFFormat.TURTLE_PRETTY)
        FileOutputStream(File("$resourcePath/add-additional-fields.ttl")).use {
            it.bufferedWriter().use { writer ->
                writer.write(turtle.second)
            }
        }
        ntriples.third.forEach {
            log.error(it)
        }
        assertAll(
            "",
            {
                assertThat(sort(ntriples.second.split("\n")))
                    .isEqualTo(readOutputFile("output-2"))
            }
        )
    }

    @Test
    fun `test date fields`() {
        val source = klaxon.parse<Map<String, Any>>(readInputFile("3-input")).orEmpty()
        val mapping = MappingConfigurationParser()
        val configuration = mapping.parse(readMapping("3-mapping"))
        val result = ResourceBuilder(
            source,
            configuration,
            headerMetadata.institutionId,
            headerMetadata.recordSetId,
            headerMetadata.isPublished
        ).extractRecordId()
            .extractRecordTypeValue()
            .generateRecord()
            .generatePhysicalObject()
            .generateDigitalObject()
            .generateThumbnailObject()
            .addDerivedConnection()

        val ntriples = result
            .writeRecord(RDFFormat.NTRIPLES_UTF8)
        val turtle = result.writeRecord(RDFFormat.TURTLE_PRETTY)
        FileOutputStream(File("$resourcePath/agents-date-fields.ttl")).use {
            it.bufferedWriter().use { writer ->
                writer.write(turtle.second)
            }
        }
        ntriples.third.forEach {
            log.error(it)
        }
        assertAll(
            "",
            {
                assertThat(sort(ntriples.second.split("\n")))
                    .isEqualTo(readOutputFile("3-output"))
            }
        )
    }
}
