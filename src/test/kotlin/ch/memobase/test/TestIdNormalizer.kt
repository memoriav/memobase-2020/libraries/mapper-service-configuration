/*
 * mapper-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.test

import ch.memobase.helpers.StringHelpers
import java.util.stream.Stream
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestIdNormalizer {

    @ParameterizedTest
    @MethodSource("idExamples")
    fun `test id normalization`(params: Pair<String, String>) {
        val result = StringHelpers.normalizeId(params.first)
        assertThat(result).isEqualTo(params.second)
    }

    private fun idExamples() = Stream.of(
        Pair("J2.143#1996/386#414-3#1*", "J2_143_1996_386_414-3_1*"),
        Pair(" IB Becker Audiovisuals-2", "IB_Becker_Audiovisuals-2"),
        Pair(" oisadi  ", "oisadi"),
        Pair("Test      mit vielen  spaces   . jal", "Test_mit_vielen_spaces_jal"),
        Pair("test___underscores", "test_underscores")
    )
}
