## Mapper Service Configuration

Releases: [Package Registry](https://gitlab.switch.ch/memoriav/memobase-2020/libraries/package-registry/-/packages)

The code to parse and use the mapper configuration. The parsed configuration can
be serialized into a simplified format to speed up deserialization. 

Confluence: [Documentation](https://ub-basel.atlassian.net/wiki/spaces/MEMOBASE/pages/29295472/Service+Mapper)
